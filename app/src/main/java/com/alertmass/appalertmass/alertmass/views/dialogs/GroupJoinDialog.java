package com.alertmass.appalertmass.alertmass.views.dialogs;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.alertmass.appalertmass.alertmass.util.StringToSHA;
import com.alertmass.appalertmass.alertmass.views.OnGroupActionSuccessListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GroupJoinDialog extends DialogFragment implements View.OnClickListener {

    private static final String TAG = GroupJoinDialog.class.getSimpleName();
    private EditText mail;
    private EditText password;
    private Context context;
    private OnGroupActionSuccessListener listener;
    private OnGroupActionSuccessListener onGroupActionSuccessListener;

    public static GroupJoinDialog newInstance(Context context) {
        GroupJoinDialog dialogJoinGroup = new GroupJoinDialog();
        dialogJoinGroup.setContext(context);
        return dialogJoinGroup;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setOnSuccessListener(OnGroupActionSuccessListener listener) {
        this.listener = listener;
    }
    public void setOnGroupActionSuccessListener(OnGroupActionSuccessListener listener) {
        this.onGroupActionSuccessListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_group_join, null);
        //Set views
        Button btnJoin = (Button) view.findViewById(R.id.btnJoin);
        Button btnCancel = (Button) view.findViewById(R.id.btnClose);
        password = (EditText) view.findViewById(R.id.txtGroupName);
        mail = (EditText) view.findViewById(R.id.txtDescription);
        //Set fonts
        FontUtil.setFontBook(getActivity(), mail);
        FontUtil.setFontBook(getActivity(), password);
        FontUtil.setFontBook(getActivity(), btnJoin);
        FontUtil.setFontBook(getActivity(), btnCancel);
        btnJoin.setOnClickListener(this);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }
        });
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public void addGroup(String pass, String mail) {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(getResources().getString(R.string.loading));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        SharedPreferences preferencesMail = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preferencesMail.edit().putString("Mail", mail).apply();
        StringToSHA.encryptPassword(pass);
        ParseQuery<ParseObject> queryGrupo = ParseQuery.getQuery("canalpri");
        queryGrupo.whereEqualTo("pass", StringToSHA.encryptPassword(pass));
        queryGrupo.whereEqualTo("email", mail);

        queryGrupo.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                pDialog.dismiss();
                if (e == null) {
                    if (list.size() == 0) {
                        Toast.makeText(context, R.string.wrong_credentials, Toast.LENGTH_SHORT).show();
                    } else {
                        ParseObject grupo = list.get(0);
                        if (grupo.getBoolean("status")) {
                            subscribeGroup(grupo);
                        } else {
                            Toast.makeText(context, R.string.disabled_group, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(context, R.string.error_credentials, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void subscribeGroup(final ParseObject group) {
        if (ParseLocalStorage.getGroupById(group.getString(Group.ID)) == null) {
            ParsePush.subscribeInBackground("as" + group.getString(Group.ID), new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Toast.makeText(context, R.string.succesful_group_subscription, Toast.LENGTH_SHORT).show();
                        ParseLocalStorage.saveGroupObject(group);
                        updateInstallation(group);
                    } else {
                        Log.e(TAG, "done: ", e);
                        Toast.makeText(context, R.string.fail_group_subscription, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else {
            Toast.makeText(context, "El grupo ya ha sido agregado previamente.", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateInstallation(ParseObject grupo) {
        String id = ParseInstallation.getCurrentInstallation().getObjectId();
        grupo.addUnique("idinstall", id);
        grupo.saveInBackground();
        listener.onSuccess();
    }

    public boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnJoin:
                if (NetworkUtil.checkEnabledInternet(getActivity())) {
                    String pass = password.getText().toString();
                    String email = mail.getText().toString();
                    if (pass.isEmpty() || email.isEmpty()) {
                        Toast.makeText(getActivity(), R.string.fill_fields, Toast.LENGTH_SHORT).show();
                    } else if (validateEmail(email)) {
                        addGroup(pass, email);
                        dismiss();
                    } else {
                        Toast.makeText(getActivity(), R.string.enter_valid_email, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
