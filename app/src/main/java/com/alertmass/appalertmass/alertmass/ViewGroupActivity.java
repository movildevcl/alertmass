package com.alertmass.appalertmass.alertmass;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.views.dialogs.GroupDetailDialog;
import com.alertmass.appalertmass.alertmass.views.dialogs.GroupImageDialog;
import com.alertmass.appalertmass.alertmass.views.dialogs.GroupInviteDialog;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ViewGroupActivity extends AppCompatActivity {


    private ParseObject group;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1;
    private static final String TAG = GroupDetailDialog.class.getSimpleName();
    private static final int SELECT_FILE = 2;
    private Uri imgUri;
    private String imgPath;
    ImageButton imgBtnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_group);
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        ImageButton imgBtnBack2 = (ImageButton)findViewById(R.id.imgBtnBack2);
        Bundle bundle = getIntent().getExtras();
        String groupId = bundle.getString("groId");
        group = ParseLocalStorage.getGroupById(groupId);
        TextView name = (TextView) findViewById(R.id.txtGroupName);
        name.setText(group.getString(Group.NAME));
        imgBtnBack2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });
        imgBtnAdd = (ImageButton)findViewById(R.id.imgBtnAdd);
        imgBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupInviteDialog groupInviteDialog = GroupInviteDialog.newInstance(group);
                groupInviteDialog.show(getSupportFragmentManager(), "groupInviteDialog");
            }
        });
    }

}
