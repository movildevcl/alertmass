package com.alertmass.appalertmass.alertmass.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Channel;
import com.alertmass.appalertmass.alertmass.data.Country;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Adapter for subscribed channels RecyclerView.
 */
public class AdapterChannels extends RecyclerView.Adapter {

    private TextView emptyView;
    private Context context;
    private List<ParseObject> channels;

    public AdapterChannels(TextView emptyView, Context context) {
        try {
            this.emptyView = emptyView;
            this.context = context;
            this.channels = ParseLocalStorage.getChannels();
        }catch (Exception ignored){}
    }

    public void unsuscribeChannel(final ParseObject channel) {
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.loading));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        ParsePush.unsubscribeInBackground("as" + channel.getObjectId(), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Toast.makeText(context, context.getString(R.string.channel_unsubscribe_fail) + " " + channel.getString(Channel.NAME), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, context.getString(R.string.channel_have_unsubscribed) + " " + channel.getString(Channel.NAME), Toast.LENGTH_SHORT).show();
                    ParseLocalStorage.removeChannel(channel.getObjectId());
                    updateChannels();
                }
                pDialog.dismiss();
            }
        });
    }

    public void updateChannels() {
        channels = ParseLocalStorage.getChannels();
        Collections.sort(channels, new Comparator<ParseObject>() {
            @Override
            public int compare(ParseObject lhs, ParseObject rhs) {
                return lhs.getString(Channel.NAME).compareTo(rhs.getString(Channel.NAME));
            }
        });
        notifyDataSetChanged();
        checkEmptyness();
    }

    public void checkEmptyness() {
        if (getItemCount() == 0 && emptyView.getVisibility() == View.GONE) {
            emptyView.setVisibility(View.VISIBLE);
        } else if (getItemCount() > 0 && emptyView.getVisibility() == View.VISIBLE) {
            emptyView.setVisibility(View.GONE);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_canales, parent, false);
        return new CanalesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String canalId = channels.get(position).getObjectId();
        ((CanalesViewHolder) holder).bindCanal(canalId);
    }

    @Override
    public int getItemCount() {
        if (channels == null) {
            return 0;
        } else {
            return channels.size();
        }
    }

    class CanalesViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView title;
        private TextView subtitle;
        private TextView txtCountry;
        private ImageView deleteButton;

        public CanalesViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imgCanal);
            title = (TextView) itemView.findViewById(R.id.txtCanal);
            subtitle = (TextView) itemView.findViewById(R.id.txtSubCanal);
            txtCountry = (TextView)itemView.findViewById(R.id.txtPais);
            deleteButton = (ImageView) itemView.findViewById(R.id.btnEliminarCanal);
        }

        public void bindCanal(String channelId) {
            String country;
            ParseObject canalLocalStorage = ParseLocalStorage.getChannelById(channelId);
            if (canalLocalStorage != null) {
                country = Country.getCurrentCountry(context);
                System.out.println(AppConfig.CHANNEL_IMAGE_SERVER + channelId);
                Picasso.with(itemView.getContext())
                        .load(AppConfig.CHANNEL_IMAGE_SERVER + channelId)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        //.centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
                title.setText(canalLocalStorage.getString(Channel.NAME));
                subtitle.setText(canalLocalStorage.getString(Channel.DESCRIPTION));
                txtCountry.setText(country);
                deleteButton.setTag(canalLocalStorage);
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {

                        new AlertDialog.Builder(view.getContext())
                                .setTitle(view.getResources().getString(R.string.delete_channel))
                                .setMessage(view.getResources().getString(R.string.confirm_delete_channel))
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        if (NetworkUtil.checkEnabledInternet(view.getContext())) {
                                            final ParseObject idbtn = (ParseObject) view.getTag();
                                            unsuscribeChannel(idbtn);
                                        } else {
                                            Toast.makeText(view.getContext(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();
                    }
                });
            }
        }
    }
}
