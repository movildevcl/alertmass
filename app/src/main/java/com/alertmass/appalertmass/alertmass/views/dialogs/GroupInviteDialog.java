package com.alertmass.appalertmass.alertmass.views.dialogs;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by benjamin on 2/12/16.
 */
public class GroupInviteDialog extends DialogFragment implements View.OnClickListener {
    private static final String TAG = GroupInviteDialog.class.getSimpleName();
    private Button btnCancel;
    private Button btnInvite;
    private Button btnInviteAllUsers;
    private EditText email;
    private EditText name;
    private ParseObject group;


    public static GroupInviteDialog newInstance(ParseObject group) {
        GroupInviteDialog alertSendDialog = new GroupInviteDialog();
        alertSendDialog.group = group;
        return alertSendDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_group_invite, null);
        email = (EditText) view.findViewById(R.id.txtEmail);
        name = (EditText) view.findViewById(R.id.txtName);
        btnInvite = (Button) view.findViewById(R.id.btnInvite);
        btnCancel = (Button) view.findViewById(R.id.btnClose);
        btnInviteAllUsers = (Button) view.findViewById(R.id.btnInviteAllUsers);
        btnInvite.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }
        });
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    private void inviteUser() throws JSONException {
        OkHttpClient client = new OkHttpClient();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("idgrupo", group.getString(Group.ID));
        jsonObject.put("nomgrupo", group.getString(Group.NAME));
        jsonObject.put("desgrupo", group.getString(Group.DESC));
        jsonObject.put("email", email.getText().toString());
        jsonObject.put("nomper", name.getText().toString());
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Request request = new Request.Builder()
                .addHeader("Content-type", "application/json")
                .url("http://api1.dev-cuchufli.com")
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "onFailure: ", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.i(TAG, "onResponse: " + response.body().string());
                response.body().close();
            }
        });

    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.btnInvite:
                if (NetworkUtil.checkEnabledInternet(view.getContext())) {
                    if(!email.getText().toString().isEmpty()) {
                        try {
                            inviteUser();
                        } catch (JSONException e) {
                            Log.e(TAG, "onClick: ", e);
                        }
                        dismiss();
                    }
                    else {
                        Toast.makeText(getActivity(), R.string.fill_fields, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnClose:
                dismiss();
                break;
        }
    }

}
