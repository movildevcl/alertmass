package com.alertmass.appalertmass.alertmass.views.tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.adapters.AdapterAlerts;
import com.alertmass.appalertmass.alertmass.adapters.DividerItemDecoration;

/**
 * Shows list of alerts the user has received.
 */
public class AlertsTabFragment extends Fragment {
    private RecyclerView.Adapter adapter;
    private String TAG = "AlertsTabFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_alerts, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView recyclerViewAlerts = (RecyclerView) getView().findViewById(R.id.recViewAlerts);
        recyclerViewAlerts.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewAlerts.setLayoutManager(layoutManager);
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        recyclerViewAlerts.addItemDecoration(itemDecoration);
        adapter = new AdapterAlerts();
        recyclerViewAlerts.setAdapter(adapter);
    }

    public void notifyAdapter() {
        ((AdapterAlerts) adapter).updateAlerts();
    }

    @Override
    public void onResume() {
        super.onResume();
        notifyAdapter();
    }

}
