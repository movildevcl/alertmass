package com.alertmass.appalertmass.alertmass.data;

import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;

/**
 * Class that contains static fields and methods for dealing with channels.
 */

public class Channel {

    public static final String TABLE_NAME = "canales";
    /**
     * Static fields that correspond to fields in the Parse DB table
     */
    public static final String NAME = "nomcanal";
    public static final String DESCRIPTION = "descanal";
    public static final String CATEGORY_ID = "idcat";
    public static final String STATUS = "status";

    public static int getSubscribersCount(String objectId) throws ParseException {
        int count;
        HashMap<String, String> params = new HashMap<>();
        if (objectId.substring(0, 1).equals("as")) {
            params.put("channel", objectId);
            params.put(STATUS, String.valueOf(true));
            //count = ParseCloud.callFunction("countSubscribers", params);
        } else {
            params.put("channel", "as" + objectId);
            //count = ParseCloud.callFunction("countSubscribers", params);
        }
        return 0;
    }


}
