package com.alertmass.appalertmass.alertmass.adapters;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Alert;
import com.alertmass.appalertmass.alertmass.data.DataHolder;
import com.alertmass.appalertmass.alertmass.data.DateSection;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.views.AlertMessageActivity;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for received alerts RecyclerView
 */
public class AdapterAlerts extends RecyclerView.Adapter {

    String TAG = "AdapterAlerts";
    private List<ParseObject> alerts;

    public AdapterAlerts() {
        try {
            alerts = Alert.getAlerts();
        } catch (ParseException e) {
            e.printStackTrace();
            alerts = new ArrayList<>();
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_alerta, parent, false);
        return new AlertsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ParseObject alert = alerts.get(position);
        ((AlertsViewHolder) holder).bindAlert(alert, alert.getBoolean(Alert.HAS_HEADER), alert.getBoolean(Alert.IS_GROUP));
    }

    @Override
    public int getItemCount() {
        return alerts.size();
    }

    /**
     * Gets alerts from Parse local datastore. If there are not, initializes an empty ArrayList.
     */
    public void updateAlerts() {
        try {
            alerts = Alert.getAlerts();
        } catch (ParseException e) {
            e.printStackTrace();
            alerts = new ArrayList<>();
        }
        notifyDataSetChanged();
    }

    /**
     * Deletes alert. If section has more alerts, reduce count. Else, remove section. If alert has header and section has more alerts, set header to true for the next alert.
     *
     * @param alert Alert
     * @throws ParseException
     */
    private void deleteAlert(ParseObject alert) throws ParseException {
        ParseQuery<ParseObject> querySection = ParseQuery.getQuery(DateSection.TABLE_NAME);
        querySection.fromLocalDatastore();
        querySection.whereEqualTo(DateSection.DATE, alert.get(Alert.DATE));
        ParseObject section = querySection.getFirst();
        int count = section.getInt(DateSection.COUNT);
        boolean hasHeader = alert.getBoolean(Alert.HAS_HEADER);
        alert.unpin();
        if (count > 1) {
            section.remove(DateSection.COUNT);
            section.put(DateSection.COUNT, --count);
            if (hasHeader) {
                ParseQuery<ParseObject> oldAlertQuery = new ParseQuery<>(Alert.TABLE_NAME);
                oldAlertQuery.fromLocalDatastore();
                oldAlertQuery.orderByDescending(Alert.TIMESTAMP);
                ParseObject oldAlert = oldAlertQuery.getFirst();
                oldAlert.remove(Alert.HAS_HEADER);
                oldAlert.put(Alert.HAS_HEADER, true);
            }
        } else {
            section.unpin();
        }
    }

    /**
     * ViewHolder
     */
    class AlertsViewHolder extends RecyclerView.ViewHolder {

        TextView header;
        ImageView img;
        TextView title;
        TextView body;
        TextView date;

        AlertsViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            img = (ImageView) itemView.findViewById(R.id.imgNoti);
            title = (TextView) itemView.findViewById(R.id.txtNoti);
            body = (TextView) itemView.findViewById(R.id.txtSubNoti);
            date = (TextView) itemView.findViewById(R.id.txtFechaEnv);
            header = (TextView) itemView.findViewById(R.id.txtHeader);
        }

        void bindAlert(final ParseObject alert, boolean hasHeader, boolean isGroup) {
            //Check if alert has date section header
            if (hasHeader) {
                header.setVisibility(View.VISIBLE);
                SimpleDateFormat input = Alert.DATE_FORMAT_INPUT;
                SimpleDateFormat output = Alert.DATE_FORMAT_OUTPUT;
                try {
                    header.setText(output.format(input.parse(alert.getString(Alert.DATE))));
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            } else {
                header.setVisibility(View.GONE);
            }
            //Set image
            Context context = itemView.getContext();

            String foo = alert.get("idcanal").toString();


            String substr = foo.substring(2,foo.length());

            if (substr.length()>=9) {

                ContextWrapper cw = new ContextWrapper(context);
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                File myImageFile = new File(directory,substr+"my_image.jpeg");
                Picasso.with(context).load(myImageFile)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            } else {
                Picasso.with(context)
                        .load(AppConfig.CHANNEL_IMAGE_SERVER + alert.get(Alert.CHANNEL_ID))
                        .transform(new CircleTransform(itemView.getContext().getResources().getColor(R.color.lightgrey)))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            }

            title.setText(alert.getString(Alert.CHANNEL_NAME));
            //Shorten long message body
            int textLimit = 60;
            String bodyText = alert.getString(Alert.MSG);
            if (bodyText.length() > textLimit) {
                bodyText = bodyText.substring(0, textLimit) + "...";
            }
            body.setText(bodyText);
            //Set to bold if alert has not been read and then flag it as read. Else, set typeface to normal.
            if (alert.get(Alert.IS_READ) == Boolean.FALSE) {
                title.setTypeface(title.getTypeface(), Typeface.BOLD);
                body.setTypeface(body.getTypeface(), Typeface.BOLD);
            } else {
                title.setTypeface(null, Typeface.NORMAL);
                body.setTypeface(null, Typeface.NORMAL);
            }
            //Format date
            SimpleDateFormat input = Alert.TIME_FORMAT_INPUT;
            SimpleDateFormat output = Alert.TIME_FORMAT_OUTPUT;
            try {
                date.setText(input.format(input.parse(alert.getString(Alert.HOUR))));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            itemView.setTag(alert);
            //Add listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), AlertMessageActivity.class);
                    DataHolder.getInstance().setData(alert);
                    itemView.getContext().startActivity(intent);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
                    new AlertDialog.Builder(v.getContext())
                            .setTitle(v.getResources().getString(R.string.delete))
                            .setMessage(v.getResources().getString(R.string.alert_delete_confirm))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    try {
                                        deleteAlert(alert);
                                        updateAlerts();
                                        Toast.makeText(v.getContext(), R.string.alert_deleted, Toast.LENGTH_SHORT).show();
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                    return false;
                }
            });
        }
    }

}
