package com.alertmass.appalertmass.alertmass.database;

import android.util.Log;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.data.Channel;
import com.alertmass.appalertmass.alertmass.data.Country;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Class with static fields and methods for managing the Parse Local Datastore.
 */
public class ParseLocalStorage {

    private static final String TAG = ParseLocalStorage.class.getSimpleName();

    public static boolean saveChannelObject(ParseObject channel) {
        try {
            channel.put(Country.NAME, AppConfig.country);
            channel.pin();
            return true;
        } catch (ParseException e) {
            Log.e(TAG, "saveChannelObject: ", e);
        }
        return false;
    }

    public static boolean saveGroupObject(ParseObject group) {
        try {
            group.put(Country.NAME, AppConfig.country);
            group.pin();
            return true;
        } catch (ParseException e) {
            Log.e(TAG, "saveGroupObject: ", e);
        }
        return false;
    }

    public static ParseObject getChannelById(String id) {

        try {
            ParseQuery<ParseObject> queryChannels = ParseQuery.getQuery(Channel.TABLE_NAME);
            queryChannels.fromLocalDatastore();
            return queryChannels.get(id);
        } catch (ParseException e) {
            Log.e(TAG, "getChannelById: ", e);

        }
        return null;
    }

    public static ParseObject getGroupById(String id) {

        try {
            ParseQuery<ParseObject> queryGroups = ParseQuery.getQuery(Group.TABLE_NAME);
            queryGroups.fromLocalDatastore();
            queryGroups.whereEqualTo(Group.ID, id);
            return queryGroups.getFirst();
        } catch (ParseException e) {
            Log.e(TAG, "getGroupById: ", e);
        }
        return null;
    }


    public static void removeChannel(String id) {

        try {
            ParseQuery<ParseObject> queryChannels = ParseQuery.getQuery(Channel.TABLE_NAME);
            queryChannels.fromLocalDatastore();
            queryChannels.get(id).unpin();
        } catch (ParseException e) {
            Log.e(TAG, "removeChannel: ", e);
        }
    }

    public static void removeGroup(String id) {

        try {
            ParseQuery<ParseObject> queryChannels = ParseQuery.getQuery(Group.TABLE_NAME);
            queryChannels.fromLocalDatastore();
            queryChannels.whereEqualTo(Group.ID, id);
            queryChannels.getFirst().unpin();
        } catch (ParseException e) {
            Log.e(TAG, "removeGroup: ", e);
        }
    }

    public static List<ParseObject> getChannels() {

        try {
            ParseQuery<ParseObject> queryChannels = ParseQuery.getQuery(Channel.TABLE_NAME);
            queryChannels.fromLocalDatastore();
            return queryChannels.find();
        } catch (ParseException e) {
            Log.e(TAG, "getChannelsAndGroups: ", e);
        }
        return new ArrayList<>();
    }

    public static List<ParseObject> getGroups() {

        try {
            ParseQuery<ParseObject> queryChannels = ParseQuery.getQuery(Group.TABLE_NAME);
            queryChannels.fromLocalDatastore();
            return queryChannels.find();
        } catch (ParseException e) {
            Log.e(TAG, "getChannelsAndGroups: ", e);
        }
        return new ArrayList<>();
    }
}
