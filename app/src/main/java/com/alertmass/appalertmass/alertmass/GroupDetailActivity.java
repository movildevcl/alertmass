package com.alertmass.appalertmass.alertmass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.BitmapUtil;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.ExifUtil;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.views.dialogs.AlertSendDialog;
import com.alertmass.appalertmass.alertmass.views.dialogs.GroupDetailDialog;
import com.alertmass.appalertmass.alertmass.views.dialogs.GroupImageDialog;
import com.parse.ParseObject;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class GroupDetailActivity extends AppCompatActivity {

    private static final String TAG = GroupDetailDialog.class.getSimpleName();
    private static final int SELECT_FILE = 2;
    private ParseObject group;
    private GroupDetailDialog.OnGroupChangeListener onGroupChangeListener;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1;
    private Uri imgUri;
    private ImageView img;
    private String imgPath;
    private String groupId;
    private Bundle bundle;
    private ImageButton imgBtnBack;
    Button btnViewGroup;
    Button btnNotify;
    ImageView imgLogo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        ImageButton imgBtnBack = (ImageButton)findViewById(R.id.imgBtnBack);
        bundle = getIntent().getExtras();
        groupId = bundle.getString("groId");
        group = ParseLocalStorage.getGroupById(groupId);
        btnViewGroup = (Button)findViewById(R.id.btnViewGroup);

        btnNotify = (Button)findViewById(R.id.btnNotify);
        btnNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupDetailActivity.this,SendNotifyActivity.class);
                intent.putExtra("groId", groupId);
                startActivity(intent);
            }
        });

        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        btnViewGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupDetailActivity.this,ViewGroupActivity.class);
                intent.putExtra("groId", groupId);
                startActivity(intent);
            }
        });

        imgLogo = (ImageView)findViewById(R.id.imgLogo);
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupImageDialog groupImageDialog = new GroupImageDialog();
                groupImageDialog.setCameraOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        captureImage();
                    }
                });
                groupImageDialog.setGalleryOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseFromGallery();
                    }
                });
                groupImageDialog.show(getSupportFragmentManager(), "tagGrupo");
            }
        });

        TextView name = (TextView) findViewById(R.id.txtGroupName);
        TextView description = (TextView) findViewById(R.id.txtDescription);
        img = (ImageView) findViewById(R.id.imgLogo);
        name.setText(group.getString(Group.NAME));
        description.setText(group.getString(Group.DESC));
        Picasso.with(this)
                .load(AppConfig.GROUP_IMAGE_GET_SERVER + "as" + group.getString(Group.ID))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .fit()
                //.centerCrop()
                .transform(new CircleTransform(CircleTransform.NO_BORDER))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(img);

        if (group.getBoolean(Group.IS_ADMIN)) {
            showAdminOptions();
        }

    }




    private void showAdminOptions() {
        LinearLayout adminOptions = (LinearLayout) findViewById(R.id.adminBtnGroup);
        Typeface iconFont = FontUtil.getTypeface(getApplicationContext(), FontUtil.FONTAWESOME);
        FontUtil.markAsIconContainer(adminOptions, iconFont);
        adminOptions.setVisibility(View.VISIBLE);
        btnNotify = (Button) findViewById(R.id.btnNotify);
        btnViewGroup = (Button) findViewById(R.id.btnViewGroup);

    }

    private void close() {
//      if image was changed, upload it while showing pDialog
        if (imgUri != null) {
            uploadGroupImage(AppConfig.GROUP_IMAGE_POST_SERVER, imgUri, group.getString(Group.ID));
        }
    }


    private void chooseFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }

    /**
     * Creates empty image file. Launches camera app to capture image.
     */
    private void captureImage() {
        File image = createImageFile();
        imgUri = Uri.fromFile(image);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Creates empty image file in external storage
     *
     * @return File
     */
    private File createImageFile() {
        // External location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                AppConfig.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + AppConfig.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");


        return mediaFile;
    }

    public void uploadGroupImage(String serverURL, Uri uri, String filename) {
        Log.i(TAG, "uploadGroupImage: ");
        OkHttpClient client = new OkHttpClient();
        InputStream inputStream = null;
        try {
            inputStream =getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "uploadGroupImage: ", e);
        }
        Bitmap bitmap = BitmapUtil.decodeSampledBitmapFromResourceMemOpt(inputStream, 150, 150);
        bitmap = ExifUtil.rotateBitmap(imgPath, bitmap);
        Log.i(TAG, "uploadGroupImage: " + bitmap.getWidth() + " " + bitmap.getHeight());
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        byte[] bitmapData = bytes.toByteArray();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", filename, RequestBody.create(MediaType.parse("image/jpeg"), bitmapData))
                .build();

        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .url(serverURL)
                .post(requestBody)
                .build();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            close();
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
        return super.onKeyDown(keyCode, event);
    }
    /**
     * Receiving activity result method will be called after choosing image
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                imgPath = imgUri.getPath();
                Picasso.with(this)
                        .load(imgUri)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        //.centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            } else if (requestCode == SELECT_FILE) {
                imgUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = getContentResolver().query(imgUri,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgPath = cursor.getString(columnIndex);
                cursor.close();
                Picasso.with(this)
                        .load(imgUri)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            }
        }
    }

}
