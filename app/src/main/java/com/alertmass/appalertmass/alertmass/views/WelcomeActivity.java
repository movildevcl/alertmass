package com.alertmass.appalertmass.alertmass.views;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.MainActivity;
import com.alertmass.appalertmass.alertmass.ParseManager;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Country;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.parse.ParsePush;

import java.util.Arrays;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_PHONE_STATE;


public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinnerCountries;
    private final int requestPermissionCode = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        spinnerCountries = (Spinner) findViewById(R.id.spinnerCountries);
        Button btnAcceptCountry = (Button) findViewById(R.id.btnAcceptCountry);
        TextView txtWelcomeError = (TextView) findViewById(R.id.txtWelcomeError);
        TextView txtSelectCountry = (TextView) findViewById(R.id.txtSelectCountry);
        LinearLayout layoutSpinnerCountry = (LinearLayout) findViewById(R.id.layoutSpinnerCountry);
        if (!Country.getCurrentCountry(this).isEmpty()) {
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            if (NetworkUtil.checkEnabledInternet(this)) {
                btnAcceptCountry.setVisibility(View.VISIBLE);
                loadSpinnerCountries(this, spinnerCountries);
            } else {
                txtWelcomeError.setVisibility(View.VISIBLE);
                txtWelcomeError.setText(R.string.check_internet);
                txtWelcomeError.setTextColor(Color.WHITE);
                txtSelectCountry.setVisibility(View.GONE);
                layoutSpinnerCountry.setVisibility(View.GONE);
                btnAcceptCountry.setVisibility(View.GONE);
            }
        }

        btnAcceptCountry.setOnClickListener(this);
        //Set fonts
        FontUtil.setFontBook(this, txtSelectCountry, btnAcceptCountry, txtWelcomeError);
        if(!checkPermission()){
            requestPermissionsDialog();
        }
    }

    public void requestPermissionsDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Solicitud permisos");
        alert.setMessage("Alertmass requiere los siguientes permisos para su funcionamiento");

        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                requestPermission();
            }
        });
        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(),"Permiso denegado. Favor aprobar solicitud para utilizar Alertmass",Toast.LENGTH_LONG).show();
                finish();
            }
        });
        alert.show();
    }
    /**
     * Capture permissions OnResult
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case requestPermissionCode:

                if (grantResults.length > 0) {
                    boolean accessFinePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readPhonePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (accessFinePermission && readPhonePermission) {
                        Toast.makeText(WelcomeActivity.this, "Permisos Concedidos", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Permiso denegado. Favor aprobar solicitud para utilizar Alertmass",Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
                break;
        }
    }

    /**
     * Read permissions
     */
    private boolean checkPermission() {
        int locationPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int readStatePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
        return locationPermissionResult == PackageManager.PERMISSION_GRANTED &&
                readStatePermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(WelcomeActivity.this, new String[]
                {
                        ACCESS_FINE_LOCATION,
                        READ_PHONE_STATE
                }, requestPermissionCode);

    }


    public static void loadSpinnerCountries(Context context, Spinner spinner) {
        int countriesCount = Country.getCountries().size();
        String[] spinnerArray = new String[countriesCount];
        int index = 0;
        for (Map.Entry<String, String> mapEntry : Country.getCountries().entrySet()) {
            spinnerArray[index] = mapEntry.getValue();
            index++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.spinner_item, spinnerArray);
        Arrays.sort(spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition("Chile");
        if (spinnerPosition != -1) {
            spinner.setSelection(spinnerPosition);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAcceptCountry:
                String country = spinnerCountries.getSelectedItem().toString();
                new AsyncTask<String, Void, Void>() {
                    ProgressDialog pDialog = new ProgressDialog(WelcomeActivity.this);

                    @Override
                    protected void onPreExecute() {
                        pDialog.setMessage(getString(R.string.loading));
                        pDialog.setIndeterminate(false);
                        pDialog.setCancelable(false);
                        pDialog.show();
                    }

                    @Override
                    protected Void doInBackground(String... params) {
                        ParseManager.setCurrentCountry(pDialog.getContext(), params[0]);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void v) {
                        pDialog.dismiss();
                        Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                        ParsePush.subscribeInBackground("as001");
                        startActivity(intent);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);

                    }
                }.execute(country);
                break;
        }
    }
}
