package com.alertmass.appalertmass.alertmass;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.alertmass.appalertmass.alertmass.data.Alert;
import com.alertmass.appalertmass.alertmass.data.DateSection;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.views.WelcomeActivity;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * Manages Parse service and push notifications
 */
public class ParseManager extends ParsePushBroadcastReceiver {

    public static final String PARSE_DATA_KEY = "com.parse.Data";
    private static final String TAG = ParseManager.class.getSimpleName();
    private static int NOTIFICATION_ID = 1;
    private static int groupSound;
    private static int channelSound;

    public static void setCurrentCountry(final Context context, final String pais) {
        ParseInstallation.getCurrentInstallation().put("pais", pais);
        ParseQuery<ParseObject> queryPais = ParseQuery.getQuery("paises");

        queryPais.whereEqualTo("nompais", pais);
        String paisId = null;
        try {
            paisId = queryPais.getFirst().getObjectId();
            ParseInstallation.getCurrentInstallation().put("paisId", paisId);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final String finalPaisId = paisId;
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i(TAG, "Pais agregado: " + pais);
                    context.getSharedPreferences(AppConfig.PREF_NAME, 0).edit().putString("pais", pais).apply();
                    context.getSharedPreferences(AppConfig.PREF_NAME, 0).edit().putString("paisId", finalPaisId).apply();
                } else {
                    Log.e(TAG, "Error guardando pais!", e);
                }
            }
        });
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        Log.i(TAG, "onPushReceive: Recibiendo alerta...");

        ParseObject alerta = new ParseObject(Alert.TABLE_NAME);
        Bundle extras = intent.getExtras();
        String jsonData = extras.getString(PARSE_DATA_KEY);
        System.out.println(jsonData);
        Log.d("json recibido",jsonData);
        try {
            Log.e("answer", Alert.ANSWER);

            JSONObject jObject = new JSONObject(jsonData);
            Log.d("options",jObject.optString("options"));
            System.out.println(jObject.optString("options"));
            alerta.put(Alert.MSG, jObject.getString(Alert.MSG));
            alerta.put(Alert.HOUR, jObject.getString(Alert.HOUR));

            alerta.put(Alert.DATE, jObject.getString(Alert.DATE));
            alerta.put(Alert.ID_RESP,jObject.getString(Alert.ID_RESP));
            alerta.put(Alert.CHANNEL_ID, jObject.getString(Alert.CHANNEL_ID));
            alerta.put(Alert.CHANNEL_NAME, jObject.getString(Alert.CHANNEL_NAME));
            if (jObject.has(Alert.ID_ACTIVE))
                alerta.put(Alert.ID_ACTIVE, jObject.getString(Alert.ID_ACTIVE));
            if (jObject.has(Alert.IMG_URL))
                alerta.put(Alert.IMG_URL, jObject.getString(Alert.IMG_URL));
            if (jObject.has(Alert.OPTIONS))
                alerta.put(Alert.OPTIONS, jObject.getString(Alert.OPTIONS));
            if (jObject.has(Alert.VIDEO_URL))
                alerta.put(Alert.VIDEO_URL, jObject.getString(Alert.VIDEO_URL));
            if (jObject.has(Alert.AUDIO_URL))
                alerta.put(Alert.AUDIO_URL, jObject.getString(Alert.AUDIO_URL));
            alerta.put(Alert.IS_READ, false);
            alerta.put(Alert.TIMESTAMP, new Date());
            //Check if group or channel
            boolean isGroup = Group.isGroup(jObject.getString(Alert.CHANNEL_ID));
            if (isGroup) {
                Log.i(TAG, "onPushReceive: Group");
                alerta.put(Alert.IS_GROUP, true);
            } else {
                Log.i(TAG, "onPushReceive: Channel");
                alerta.put(Alert.IS_GROUP, false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Check if date section already exists
        ParseQuery<ParseObject> query = ParseQuery.getQuery(DateSection.TABLE_NAME);
        query.fromLocalDatastore();
        query.whereEqualTo(DateSection.DATE, alerta.get(Alert.DATE));
        ParseObject dateSection = null;
        // If it exists, set alert HAS_HEADER flag to true and increment count in DateSection
        try {
            dateSection = query.getFirst();
            dateSection.put(DateSection.COUNT, dateSection.getInt(DateSection.COUNT) + 1);
            System.out.println("Date already exists...");
            alerta.put(Alert.HAS_HEADER, true);
            // Set last alert HAS_HEADER flag to false
            ParseQuery<ParseObject> oldAlertQuery = new ParseQuery<>(Alert.TABLE_NAME);
            oldAlertQuery.fromLocalDatastore();
            oldAlertQuery.orderByDescending(Alert.TIMESTAMP);
            List<ParseObject> oldAlertlist = oldAlertQuery.find();
            ParseObject oldAlert = oldAlertlist.get(0);
            System.out.println(oldAlert.getString(Alert.MSG));
            oldAlert.remove(Alert.HAS_HEADER);
            oldAlert.put(Alert.HAS_HEADER, false);
            oldAlert.pin();
            System.out.println(oldAlert.get(Alert.HAS_HEADER));
        } catch (ParseException e) {
            Log.w(TAG, "onPushReceive: No query results.");
        }
        //Else
        if (dateSection == null) {
            System.out.println("New date...");
            dateSection = new ParseObject(DateSection.TABLE_NAME);
            dateSection.put(DateSection.DATE, alerta.get(Alert.DATE));
            dateSection.put(DateSection.COUNT, 1);
            alerta.put(Alert.HAS_HEADER, true);
            try {
                dateSection.pin();
                System.out.println(dateSection.get(DateSection.DATE));
            } catch (ParseException e) {
                Log.e(TAG, "onPushReceive: Error saving DateSection!", e);
            }
        }
        //Save alert
        try {
            alerta.pin();
        } catch (ParseException e) {
            Log.e(TAG, "onPushReceive: Error saving alert!", e);
        }

        try {
            notificar(context, intent);
            sendBroadCast(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void sendBroadCast(Context context) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("CL.ALERTMASS.PUSH");
        context.sendBroadcast(broadcastIntent);
    }

    private void notificar(Context context, Intent intent) throws JSONException {
        Bundle extras = intent.getExtras();
        String jsonData = extras.getString(PARSE_DATA_KEY);
        JSONObject jObject = new JSONObject(jsonData);

        String title = jObject.getString(Alert.CHANNEL_NAME);
        String body = jObject.getString(Alert.MSG);

        //http://stackoverflow.com/questions/24526429/parse-com-android-custom-push-notification-sound
        //Check if alert comes from group or channel, and set property and corresponding sound
        Uri sound;
        boolean isGroup = Group.isGroup(jObject.getString(Alert.CHANNEL_ID));
        if (isGroup) {
            sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + groupSound);
        } else {
            sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + channelSound);
        }

        Intent notifyIntent =
                new Intent(context, WelcomeActivity.class);
// Sets the Activity to start in a new, empty task
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
// Creates the PendingIntent
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        notifyIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


       /* Intent resultIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
          // Adds the back stack
        stackBuilder.addParentStack(WelcomeActivity.class);
            // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
            // Gets a PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
*/
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.ic_stat_logo_solo);
        builder.setContentTitle(title);
        builder.setContentText(body);
        builder.setAutoCancel(true);
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setSound(sound);
        builder.setContentIntent(resultPendingIntent);

        /*Notification note = builder.build();
        note.defaults |= Notification.DEFAULT_VIBRATE;
        note.defaults |= Notification.DEFAULT_SOUND;*/

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(++NOTIFICATION_ID, builder.build());
    }


}
