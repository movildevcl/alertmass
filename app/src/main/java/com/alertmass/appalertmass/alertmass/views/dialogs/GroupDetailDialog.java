package com.alertmass.appalertmass.alertmass.views.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.BitmapUtil;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.ExifUtil;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.parse.ParseObject;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Dialog that shows particular group info and options
 */
public class GroupDetailDialog extends DialogFragment implements View.OnClickListener {


    private static final String TAG = GroupDetailDialog.class.getSimpleName();
    private static final int SELECT_FILE = 2;
    private ParseObject group;
    private OnGroupChangeListener onGroupChangeListener;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1;
    private Uri imgUri;
    private ImageView img;
    private String imgPath;

    public static GroupDetailDialog newInstance(Context context, String groupId) {
        GroupDetailDialog groupDetailDialog = new GroupDetailDialog();
        groupDetailDialog.group = ParseLocalStorage.getGroupById(groupId);
        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("groupId", groupId).commit();
        return groupDetailDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (group == null) {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(AppConfig.PREF_NAME, 0);
            String groupId = sharedPreferences.getString("groupId", "");
            group = ParseLocalStorage.getGroupById(groupId);
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_group_detail, null);
        TextView name = (TextView) view.findViewById(R.id.txtGroupName);
        TextView description = (TextView) view.findViewById(R.id.txtDescription);
        img = (ImageView) view.findViewById(R.id.imgLogo);
        name.setText(group.getString(Group.NAME));
        description.setText(group.getString(Group.DESC));
        Picasso.with(view.getContext())
                .load(AppConfig.GROUP_IMAGE_GET_SERVER + "as" + group.getString(Group.ID))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .fit()
                .centerCrop()
                .transform(new CircleTransform(CircleTransform.NO_BORDER))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(img);

        if (group.getBoolean(Group.IS_ADMIN)) {
            showAdminOptions(view);
        }
        final Button btnCancel = (Button) view.findViewById(R.id.btnClose);
        btnCancel.setOnClickListener(this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    btnCancel.callOnClick();
                }
                return false;
            }
        });
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    private void showAdminOptions(View view) {
        LinearLayout adminOptions = (LinearLayout) view.findViewById(R.id.adminBtnGroup);
        Typeface iconFont = FontUtil.getTypeface(view.getContext().getApplicationContext(), FontUtil.FONTAWESOME);
        FontUtil.markAsIconContainer(adminOptions, iconFont);
        adminOptions.setVisibility(View.VISIBLE);
        Button btnNotify = (Button) view.findViewById(R.id.btnNotify);
        Button btnCamera = (Button) view.findViewById(R.id.btnChangeImage);
        Button btnAddUser = (Button) view.findViewById(R.id.btnAddUser);
        btnNotify.setOnClickListener(this);
        btnCamera.setOnClickListener(this);
        btnAddUser.setOnClickListener(this);
    }

    private void close() {
//      if image was changed, upload it while showing pDialog
        if (imgUri != null) {
            uploadGroupImage(AppConfig.GROUP_IMAGE_POST_SERVER, imgUri, group.getString(Group.ID));
        }
        dismiss();
    }

    public void setOnGroupChangeListener(OnGroupChangeListener listener) {
        this.onGroupChangeListener = listener;
    }

    private void chooseFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }

    /**
     * Creates empty image file. Launches camera app to capture image.
     */
    private void captureImage() {
        File image = createImageFile();
        imgUri = Uri.fromFile(image);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Creates empty image file in external storage
     *
     * @return File
     */
    private File createImageFile() {
        // External location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                AppConfig.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + AppConfig.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");


        return mediaFile;
    }

    public void uploadGroupImage(String serverURL, Uri uri, String filename) {
        Log.i(TAG, "uploadGroupImage: ");
        OkHttpClient client = new OkHttpClient();
        InputStream inputStream = null;
        try {
            inputStream = getActivity().getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "uploadGroupImage: ", e);
        }
        Bitmap bitmap = BitmapUtil.decodeSampledBitmapFromResourceMemOpt(inputStream, 150, 150);
        bitmap = ExifUtil.rotateBitmap(imgPath, bitmap);
        Log.i(TAG, "uploadGroupImage: " + bitmap.getWidth() + " " + bitmap.getHeight());
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        byte[] bitmapData = bytes.toByteArray();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", filename, RequestBody.create(MediaType.parse("image/jpeg"), bitmapData))
                .build();

        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .url(serverURL)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            Activity activity = getActivity();

            @Override
            public void onFailure(Call call, IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, R.string.group_img_upload_error, Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e(TAG, "onFailure: ", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.i(TAG, "onResponse: " + response.body().string());
                if (response.isSuccessful()) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (onGroupChangeListener != null)
                                onGroupChangeListener.onChange();
                        }
                    });
                }
            }
        });
    }

    /**
     * Receiving activity result method will be called after choosing image
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                imgPath = imgUri.getPath();
                Picasso.with(getActivity())
                        .load(imgUri)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            } else if (requestCode == SELECT_FILE) {
                imgUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(imgUri,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgPath = cursor.getString(columnIndex);
                cursor.close();
                Picasso.with(getActivity())
                        .load(imgUri)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            }
        }
    }

    /**
     * onClick listeners
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (getId()) {
            case R.id.btnNotify:
                AlertSendDialog alertSendDialog = AlertSendDialog.newInstance(group);
                alertSendDialog.show(getFragmentManager(), "alertSendDialog");
                break;
            case R.id.btnChangeImage:
                GroupImageDialog groupImageDialog = new GroupImageDialog();
                groupImageDialog.setCameraOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        captureImage();
                    }
                });
                groupImageDialog.setGalleryOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseFromGallery();
                    }
                });
                groupImageDialog.show(getFragmentManager(), "tagGrupo");
                break;
            case R.id.btnAddUser:
                GroupInviteDialog groupInviteDialog = GroupInviteDialog.newInstance(group);
                groupInviteDialog.show(getFragmentManager(), "groupInviteDialog");
                break;
            case R.id.btnClose:
                close();
                break;
        }
    }

    public interface OnGroupChangeListener {
        void onChange();
    }
}
