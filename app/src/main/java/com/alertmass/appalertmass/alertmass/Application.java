package com.alertmass.appalertmass.alertmass;


import android.content.Context;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import io.fabric.sdk.android.Fabric;


public class Application extends android.app.Application {
    private String TAG = "Application";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        try {
            Parse.initialize(new Parse.Configuration.Builder(this)
                            .applicationId(AppConfig.PARSE_APPLICATION_ID)
                            .clientKey(AppConfig.PARSE_CLIENT_KEY)
                            .server(AppConfig.SERVER)
                            .enableLocalDataStore()
                            .build()
            );
            ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        Log.e(TAG, "done: ", e);
                    }
                    Log.i(TAG, "done: "+ParseInstallation.getCurrentInstallation().getObjectId());
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "onCreate: ", e);
        }
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
