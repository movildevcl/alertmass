package com.alertmass.appalertmass.alertmass;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.data.Alert;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.alertmass.appalertmass.alertmass.views.dialogs.AlertSendDialog;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendNotifyActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = AlertSendDialog.class.getSimpleName();
    private Button btnCancel;
    private Button btnSend;
    private EditText alert;
    private ParseObject group;
    private ImageView img;
    private String groupId;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notify);

        bundle = getIntent().getExtras();
        groupId = bundle.getString("groId");
        group = ParseLocalStorage.getGroupById(groupId);

        ImageButton imgBtnBack = (ImageButton)findViewById(R.id.imgBtnBack);
        alert = (EditText) findViewById(R.id.txtAlert);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

        FontUtil.setFontBook(this, alert, btnSend);

        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        TextView name = (TextView) findViewById(R.id.txtGroupName);

        img = (ImageView) findViewById(R.id.imgLogo);
        name.setText(group.getString(Group.NAME));

        Picasso.with(this)
                .load(AppConfig.GROUP_IMAGE_GET_SERVER + "as" + group.getString(Group.ID))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .fit()
                //.centerCrop()
                .transform(new CircleTransform(CircleTransform.NO_BORDER))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(img);
    }

    private String sendAlert(String groupId, String data) throws ParseException, JSONException, IOException {
//        HashMap<String, String> params = new HashMap<>();
//        params.put("groupId", groupId);
//        params.put("installationId", ParseInstallation.getCurrentInstallation().getObjectId());
//        params.put("message", json);
//        return ParseCloud.callFunction("sendPushToGroup", params);
        OkHttpClient client = new OkHttpClient();
        JSONObject alertJson = new JSONObject();
        JSONObject channels = new JSONObject();
        JSONArray in = new JSONArray();
        in.put("as"+groupId);
        channels.put("$in", in);
        JSONObject where = new JSONObject();
        where.put("channels", channels);
        alertJson.put("where", where);
        JSONObject dataJson = new JSONObject(data);
        alertJson.put("data", dataJson);
        Log.i(TAG, "sendAlert: " + alertJson.toString());
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), alertJson.toString());
        Request request = new Request.Builder()
                .addHeader("X-Parse-Application-Id", AppConfig.PARSE_APPLICATION_ID)
                .addHeader("X-Parse-Master-Key", "bncJq5uDOuZYHaySpGntIRTzqFfgAb8XQ6qeYlaL")
                .addHeader("Content-Type", "application/json")
                .post(body)
                .url(AppConfig.SERVER + "push")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    //TODO: Asegurarse que el formato de fecha y hora se mantiene por siempre
    private String makeAlert(String msg) {
        JSONObject alert = new JSONObject();
        Date date = new Date();
        SimpleDateFormat dateFormat = Alert.DATE_FORMAT_INPUT;
        SimpleDateFormat timeFormat = Alert.TIME_FORMAT_INPUT;
        try {
            alert.put(Alert.DATE, dateFormat.format(date));
            alert.put(Alert.HOUR, timeFormat.format(date));
            alert.put(Alert.CHANNEL_NAME, group.get(Group.NAME));
            alert.put(Alert.CHANNEL_ID, group.get(Group.ID));
            alert.put(Alert.MSG, msg);
        } catch (JSONException e) {
            Log.e(TAG, "makeAlert: ", e);
            return "";
        }
        return alert.toString();
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                if (NetworkUtil.checkEnabledInternet(view.getContext())) {
                    new AsyncTask<String, Void, Boolean>() {
                        ProgressDialog pDialog;

                        @Override
                        protected void onPreExecute() {
                            pDialog = new ProgressDialog(view.getContext());
                            pDialog.setMessage(view.getContext().getString(R.string.sending));
                            pDialog.setIndeterminate(false);
                            pDialog.setCancelable(false);
                            pDialog.show();
                        }

                        @Override
                        protected Boolean doInBackground(String... alertMsg) {
                            try {
                                String response = sendAlert(group.getString(Group.ID), alertMsg[0]);
                                Log.i(TAG, "doInBackground: "+response);
                                return true;
                            } catch (ParseException | JSONException | IOException e) {
                                Log.e(TAG, "onClick: ", e);
                                return false;
                            }
                        }

                        @Override
                        protected void onPostExecute(Boolean success) {
                            //pDialog.dismiss();
                            //dismiss();
                            if (success) {
                                Toast.makeText(view.getContext(), R.string.alert_send_success, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(view.getContext(), R.string.alert_send_error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.execute(makeAlert(alert.getText().toString()));
                } else {
                    Toast.makeText(this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
                break;
        }
        finish();
    }
}
