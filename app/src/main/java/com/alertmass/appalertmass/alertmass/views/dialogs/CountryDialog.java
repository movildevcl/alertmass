package com.alertmass.appalertmass.alertmass.views.dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.util.FontUtil;


public class CountryDialog extends DialogFragment {

    public OnCountrySelectListener mListener;
    public EditText id;
    public Button aceptar;
    public Button cancelar;
    private Spinner spinner;

    public static CountryDialog newInstance(String[] countries) {
        CountryDialog fragment = new CountryDialog();
        Bundle bundle = new Bundle();
        bundle.putStringArray("countries", countries);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the

            mListener = (OnCountrySelectListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement OnCountrySelectListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String[] spinnerArray = getArguments().getStringArray("countries");
        spinnerArray = (spinnerArray != null) ? spinnerArray : new String[0];

        final Dialog dialog = new Dialog(getActivity());

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // layout to display

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_country, null);

        //SET VIEW'S


        spinner = (Spinner) view.findViewById(R.id.spinnerCountries);
        aceptar = (Button) view.findViewById(R.id.btnAccept);
        cancelar = (Button) view.findViewById(R.id.btnClose);

        FontUtil.setFontBook(getActivity(), aceptar);
        FontUtil.setFontBook(getActivity(), cancelar);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_black, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition("Chile");
        if (spinnerPosition != -1) {
            spinner.setSelection(spinnerPosition);
        }

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onCountrySelect(spinner.getSelectedItem().toString());

            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.setContentView(view);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // dialog.show();

        return dialog;
    }

    public interface OnCountrySelectListener {
        void onCountrySelect(String pais);
    }


}
