package com.alertmass.appalertmass.alertmass.data;

/**
 * Category model
 */
public class Category {

    public static String TABLE_NAME = "categorias";

    public static String NAME = "nomcat";
    public static String PAIS_ID = "idpais";
    public static String DESC = "descat";
    public static String STATUS = "status";

}
