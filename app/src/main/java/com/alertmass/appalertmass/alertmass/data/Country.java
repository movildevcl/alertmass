package com.alertmass.appalertmass.alertmass.data;

import android.content.Context;
import android.util.Log;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Country model
 */
public class Country {
    public static String TABLE_NAME = "paises";

    public static String NAME = "nompais";

    public static String getCurrentCountry(Context context) {
        return context.getSharedPreferences(AppConfig.PREF_NAME, 0).getString("pais", "");
//        return ParseInstallation.getCurrentInstallation().getString("pais");
//        return "Chile";
    }

    public static String getCurrentCountryId(Context context) {
        return context.getSharedPreferences(AppConfig.PREF_NAME, 0).getString("paisId", "");
//        return ParseInstallation.getCurrentInstallation().getString("paisId");
//        return "FZ0dZ8ao63";
    }

    public static Map<String, String> getCountries() {
        Map<String, String> countriesHashMap = new HashMap<>();
        final ParseQuery<ParseObject> queryCountries = ParseQuery.getQuery(TABLE_NAME);

        List<ParseObject> results;
        try {
            results = queryCountries.find();
            for (ParseObject country : results) {
                countriesHashMap.put(country.getObjectId(), (String) country.get(NAME));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return countriesHashMap;
    }
}
