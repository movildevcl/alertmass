package com.alertmass.appalertmass.alertmass;


import com.parse.ParseInstallation;

public class AppConfig {

    public static final String PARSE_APPLICATION_ID = "1FPKiWd4niGvmuFEistvBzy8QdEKRkAapW9E1ZNy";
    public static final String PARSE_CLIENT_KEY = "6HwEgL6uMh9zSGPndGdU3atLufwYwSNHwrpRQBiT";
    public static final String CHANNEL_IMAGE_SERVER = "https://img.dev-cuchufli.com/channel/";
    public static final String INSTALLATIONS_GET = "https://api0.dev-cuchufli.com/parse/installations?where=";
    public static final String IMAGE_DIRECTORY_NAME = "AlertMassImages";
    public static final String SURVEY_RESPONSE_API = "https://46jef0zr4b.execute-api.us-east-1.amazonaws.com/prod/response";
    public static final String GROUP_IMAGE_GET_SERVER = "http://img.dev-cuchufli.com/group/";
    public static final String PREF_NAME = "AlertMassPreferences";
    public static final String GROUP_IMAGE_POST_SERVER = "https://api2.dev-cuchufli.com/parse/";
    public static final String SERVER = "https://api0.dev-cuchufli.com/parse/";
    public static final String ID_GEOPOINT = "https://api0.dev-cuchufli.com/parse/installations/"+ ParseInstallation.getCurrentInstallation().getObjectId();
    public static final String CLEAN_CHANNELS = "https://api0.dev-cuchufli.com/parse/installations/";
    public static String country = "";
    public static String countryId = "";

}
