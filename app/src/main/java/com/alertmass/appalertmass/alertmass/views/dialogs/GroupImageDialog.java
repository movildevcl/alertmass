package com.alertmass.appalertmass.alertmass.views.dialogs;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.alertmass.appalertmass.alertmass.R;

/**
 * Created by benjamin on 2/10/16.
 */
public class GroupImageDialog extends DialogFragment implements View.OnClickListener {

    private View.OnClickListener cameraOnClickListener;
    private View.OnClickListener galleryOnClickListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_group_image, null);
        Button btnCamera = (Button) view.findViewById(R.id.btnCamera);
        Button btnGallery = (Button) view.findViewById(R.id.btnGallery);
        Button btnCancel = (Button) view.findViewById(R.id.btnClose);
        btnCamera.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }
        });
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public void setCameraOnClickListener(View.OnClickListener cameraOnClickListener) {
        this.cameraOnClickListener = cameraOnClickListener;
    }

    public void setGalleryOnClickListener(View.OnClickListener galleryOnClickListener) {
        this.galleryOnClickListener = galleryOnClickListener;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCamera:
                if (cameraOnClickListener != null)
                    cameraOnClickListener.onClick(view);
                dismiss();
                break;
            case R.id.btnGallery:
                if (galleryOnClickListener != null)
                    galleryOnClickListener.onClick(view);
                dismiss();
                break;
            case R.id.btnClose:
                dismiss();
                break;
        }
    }

}
