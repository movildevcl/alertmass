package com.alertmass.appalertmass.alertmass.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by benjamin on 1/19/16.
 */
public class NetworkUtil {

    public static boolean checkEnabledInternet(final Context context) {
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        for (NetworkInfo networkInfo : networkInfos) {
            if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                isConnected = true;
                break;
            }
        }
        return isConnected;
    }

    public static void downloadImageJPG(String urlFrom, String pathTo, Context context) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(urlFrom)
                .build();
        Response response = client.newCall(request).execute();
        InputStream inputStream = response.body().byteStream();
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//        URL url = new URL(urlFrom);
//        InputStream in = new BufferedInputStream(url.openStream());
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        byte[] buf = new byte[1024];
//        int n = 0;
//        while (-1 != (n = in.read(buf))) {
//            out.write(buf, 0, n);
//        }
//        out.close();
//        in.close();
//        byte[] response = out.toByteArray();
        File dir = context.getDir("img", Context.MODE_PRIVATE);
        File path = new File(dir, pathTo);
        FileOutputStream fos = new FileOutputStream(path);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//        fos.write(response);
        fos.close();
    }
}
