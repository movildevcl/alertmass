package com.alertmass.appalertmass.alertmass.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.adapters.AdapterCategories;
import com.alertmass.appalertmass.alertmass.adapters.DividerItemDecoration;
import com.alertmass.appalertmass.alertmass.data.Category;
import com.alertmass.appalertmass.alertmass.data.Country;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Shows list of channel categories.
 */
public class CategoriesActivity extends Activity {

    private static final String TAG = CategoriesActivity.class.getSimpleName();
    private RecyclerView recyclerViewCategorias;
    private TextView categoriesError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_categories);
        overridePendingTransition(R.anim.left_in, R.anim.left_out);

        recyclerViewCategorias = (RecyclerView) findViewById(R.id.recViewCategories);
        recyclerViewCategorias.addItemDecoration(new DividerItemDecoration(CategoriesActivity.this, DividerItemDecoration.VERTICAL_LIST));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewCategorias.setLayoutManager(layoutManager);
        TextView title = (TextView) findViewById(R.id.txtCategoryTitle);
        categoriesError = (TextView) findViewById(R.id.txtCategoryError);
        FontUtil.setFontBook(this, categoriesError);
        FontUtil.setFontBook(this, title);

        ImageButton back = (ImageButton) findViewById(R.id.imgBtnBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        if (NetworkUtil.checkEnabledInternet(this)) {
            new LoadCategoriesTask(this).execute();
        } else {
            categoriesError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * Loads categories corresponding to selected country.
     */
    private class LoadCategoriesTask extends AsyncTask<String, Void, Boolean> {

        private List<ParseObject> categories;
        private ProgressDialog pDialog;

        public LoadCategoriesTask(Context context) {
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage(getString(R.string.loading));
            pDialog.setIndeterminate(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... categoryId) {
            try {
                ParseQuery<ParseObject> queryCategorias = ParseQuery.getQuery(Category.TABLE_NAME);
                queryCategorias.whereEqualTo(Category.PAIS_ID, Country.getCurrentCountryId(pDialog.getContext()));
                queryCategorias.whereEqualTo(Category.STATUS, true);
                categories = queryCategorias.find();
            } catch (ParseException e) {
                Log.e(TAG, "doInBackground: ", e);
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            pDialog.dismiss();
            if (success) {
                RecyclerView.Adapter adapter = new AdapterCategories(categories);
                recyclerViewCategorias.setAdapter(adapter);
                categoriesError.setVisibility(View.GONE);
            } else {
                categoriesError.setVisibility(View.VISIBLE);
            }
        }
    }
}
