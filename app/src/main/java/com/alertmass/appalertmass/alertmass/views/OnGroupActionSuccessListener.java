package com.alertmass.appalertmass.alertmass.views;

/**
 * Created by benjamin on 1/21/16.
 */
public interface OnGroupActionSuccessListener {

    void onSuccess();

}
