package com.alertmass.appalertmass.alertmass.util;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;


public class CircleTransform implements Transformation {
    public static int NO_BORDER = Color.TRANSPARENT;
    private int borderColor;

    public CircleTransform(int borderColor) {
        this.borderColor = borderColor;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        if (source == null || source.isRecycled()) {
            return null;
        }
        int borderWidth = 4;
        int size = Math.min(source.getWidth(), source.getHeight());
        BitmapShader shader = new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        Bitmap circleBitmap = Bitmap.createBitmap(size, size, source.getConfig()); //Called getConfig() on a recycle()'d bitmap! This is undefined behavior!
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(shader);

        Canvas canvas = new Canvas(circleBitmap);
        float radius = size / 2f;
        canvas.drawCircle(radius, radius, radius, paint);

        if (source != circleBitmap) {
            source.recycle();
        }
        //border code
        if(borderColor != NO_BORDER) {
            paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(borderColor);
            paint.setStrokeWidth(borderWidth);
            paint.setAntiAlias(true);
            canvas.drawCircle(radius, radius, radius-borderWidth/2, paint);
        }
        //--------------------------------------

        if (circleBitmap != source) {
            source.recycle();
        }

        return circleBitmap;
    }
//    @Override
//    public Bitmap transform(Bitmap source) {
//        int size = Math.min(source.getWidth(), source.getHeight());
//
//        int x = (source.getWidth() - size) / 2;
//        int y = (source.getHeight() - size) / 2;
//
//        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
//        if (squaredBitmap != source) {
//            source.recycle();
//        }
//
//        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig()); //Called getConfig() on a recycle()'d bitmap! This is undefined behavior!
//
//        Canvas canvas = new Canvas(bitmap);
//        Paint paint = new Paint();
//        BitmapShader shader = new BitmapShader(squaredBitmap,
//                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
//        paint.setShader(shader);
//        paint.setAntiAlias(true);
//
//        float r = size / 2f;
//
//        if (borderColor == NO_BORDER) {
//            canvas.drawCircle(r, r, r, paint);
//            squaredBitmap.recycle();
//            return bitmap;
//        } else {
//            //Add colored border
//            paint.setShader(null);
//            paint.setStyle(Paint.Style.STROKE);
//            paint.setColor(borderColor);
//            paint.setStrokeWidth(4);
//            canvas.drawCircle(r, r, r - 4, paint);
//            squaredBitmap.recycle();
//            return bitmap;
//        }
//    }

    @Override
    public String key() {
        return "circle" + borderColor;
    }
}

