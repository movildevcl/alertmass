package com.alertmass.appalertmass.alertmass.data;

import java.util.regex.Pattern;

/**
 * Class that contains static fields and methods for dealing with groups.
 */
public class Group {

    public static final String TABLE_NAME = "canalpri";
    public static final String ID = "idgrupo";
    public static final String NAME = "nomgrupo";
    public static final String DESC = "desgrupo";
    public static final String IS_ADMIN = "isadmin";
    public static final String EMAIL = "email";
    public static final String STATUS = "status";
    public static final String ID_INSTALL = "idinstall";

    public static boolean isGroup(String id) {
        return id.length() > 12 || Pattern.matches("^\\d+$", id);
    }

}
