package com.alertmass.appalertmass.alertmass.util;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class FontUtil {

    public static final String ROOT = "fonts/";
    public static final String FONTAWESOME = ROOT + "fontawesome-webfont.ttf";

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }

    public static void markAsIconContainer(View v, Typeface typeface) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                markAsIconContainer(child, typeface);
            }
        } else if (v instanceof TextView) {
            ((TextView) v).setTypeface(typeface);
        }
    }


    public static void setFontBook(Context context, TextView... views) {

        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/AVENIRLTSTD-BOOK.ttf");
        for (TextView view : views) {
            view.setTypeface(myTypeface);
        }
    }

    public static void setFontHeavy(Context context, TextView... views) {

        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/AVENIRLTSTD-HEAVY.ttf");
        for (TextView view : views) {
            view.setTypeface(myTypeface);
        }
    }

    public static void setFontLight(Context context, TextView... views) {

        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/AVENIRLTSTD-LIGHT.ttf");
        for (TextView view : views) {
            view.setTypeface(myTypeface);
        }
    }

    public static void setFontMedium(Context context, TextView... views) {

        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/AVENIRLTSTD-MEDIUM.ttf");
        for (TextView view : views) {
            view.setTypeface(myTypeface);
        }
    }
}
