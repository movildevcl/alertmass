package com.alertmass.appalertmass.alertmass.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

import java.util.Timer;
import java.util.TimerTask;


public class AlertMassService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    double latitude; // latitude
    double longitude; // longitude
    private Location location_service; // location
    //Timer
    private Timer timerLocation = new Timer();
    private GoogleApiClient mGoogleApiClient;

    public static void updateLocationParse(double latitud, double longitud) {

        ParseGeoPoint point = new ParseGeoPoint(latitud, longitud);
        ParseInstallation.getCurrentInstallation().put("mipos", point);

        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.e("alertmass", "Ubicación actualizada");
                } else {
                    Log.e("alertmass", "Ubicación no actualizada");
                }
            }
        });
    }

    @Override
    public void onCreate() {
      /*  service = new Booking_service();
        service.addObserver(this);*/
        Log.d("Servicio", "Create");


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("Servicio", "Command");
        startGetLocation();
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (timerLocation != null) {
            timerLocation.cancel();
        }
        Log.d("Servicio", "Destruido");

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void startGetLocation() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000 * 60 * 10);
        mLocationRequest.setFastestInterval(1000 * 60 * 5);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onConnected(Bundle bundle) {
        location_service = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, createLocationRequest(), this);

        runServiceLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("Service", "Location changed");
        Log.e("Service", "Latitud " + location.getLatitude());
        Log.e("Service", "Longitud " + location.getLongitude());

        this.location_service = location;
    }

    /**
     * Function to start service location
     */

    private void runServiceLocation() {
        Log.e("Service", "Run Location");
        timerLocation = new Timer();
        timerLocation.schedule(new TimerTask() {
            @Override
            public void run() {
                updateLocationParse(getLatitude(), getLongitude());
            }
        }, 0, 1000 * 60 * 5); //20 minutos
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location_service != null) {
            latitude = this.location_service.getLatitude();
        }
        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location_service != null) {
            longitude = this.location_service.getLongitude();
        }

        // return longitude
        return longitude;
    }


}

