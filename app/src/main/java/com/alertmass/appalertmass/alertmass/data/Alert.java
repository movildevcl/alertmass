package com.alertmass.appalertmass.alertmass.data;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

/**
 * Created by benjamin on 1/18/16.
 */
public class Alert {

    public static final String ANSWER = "answer";
    public static final String IS_GROUP = "isgroup";
    public static final String ID_RESP = "idresp";
    public static final String MSG = "alert";
    public static final String HOUR = "horenv";
    public static final String DATE = "fecenv";
    public static final String CHANNEL_ID = "idcanal";
    public static final String CHANNEL_NAME = "nomcanal";
    public static final String IS_READ = "isread";
    public static final String HAS_HEADER = "header";
    public static final String AUDIO_URL = "audiourl";
    public static final String VIDEO_URL = "videourl";

    public static final String IMG_URL = "imgurl";
    public static final String TIMESTAMP = "timestamp";
    public static final String OPTIONS = "options";
    public static final String TABLE_NAME = "alertas";
    public static final String ID_ACTIVE = "idactive";
    public static final SimpleDateFormat DATE_FORMAT_INPUT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat TIME_FORMAT_INPUT = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat DATE_FORMAT_OUTPUT = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat TIME_FORMAT_OUTPUT = new SimpleDateFormat("hh:mm a");

    public static List<ParseObject> getAlerts() throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("alertas");
        query.fromLocalDatastore();
        List<ParseObject> alerts = query.find();
        Collections.reverse(alerts);
        return alerts;
    }
}
