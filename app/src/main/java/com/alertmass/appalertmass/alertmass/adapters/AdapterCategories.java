package com.alertmass.appalertmass.alertmass.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Category;
import com.alertmass.appalertmass.alertmass.data.Channel;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.views.ChannelsCategoryActivity;
import com.parse.ParseObject;

import java.util.List;

/**
 * Adapter for channel categories RecyclerView.
 */
public class AdapterCategories extends RecyclerView.Adapter {

    private List<ParseObject> categories;
    private static final String TAG = AdapterCategories.class.getSimpleName();

    public AdapterCategories(List<ParseObject> categories) {
        this.categories = categories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_categorias, parent, false);
        return new CategoriasViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ParseObject categoria = categories.get(position);
        ((CategoriasViewHolder) holder).bindCategory(categoria);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class CategoriasViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView subtitle;

        public CategoriasViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            title = (TextView) itemView.findViewById(R.id.txtCat);
            subtitle = (TextView) itemView.findViewById(R.id.txtSubCat);
            FontUtil.setFontLight(itemView.getContext(), title);
            FontUtil.setFontBook(itemView.getContext(), subtitle);

        }

        public void bindCategory(final ParseObject category) {
            title.setText(category.getString(Category.NAME));
            subtitle.setText(category.getString(Category.DESC));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), ChannelsCategoryActivity.class);
                    intent.putExtra(Category.NAME, category.getString(Category.NAME));
                    intent.putExtra(Channel.CATEGORY_ID, category.getObjectId());
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
