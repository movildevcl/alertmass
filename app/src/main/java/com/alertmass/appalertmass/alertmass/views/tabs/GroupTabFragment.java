package com.alertmass.appalertmass.alertmass.views.tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.adapters.AdapterGroups;
import com.alertmass.appalertmass.alertmass.adapters.DividerItemDecoration;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.views.OnGroupActionSuccessListener;
import com.alertmass.appalertmass.alertmass.views.dialogs.GroupActionDialog;

/**
 * Groups tab
 */
public class GroupTabFragment extends Fragment {
    private RecyclerView.Adapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_groups, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
        RecyclerView recyclerViewGrupos = (RecyclerView) getView().findViewById(R.id.recViewGrupos);
        TextView labelEmptyList = (TextView) getView().findViewById(R.id.txtGroupEmpty);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewGrupos.setLayoutManager(layoutManager);
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        recyclerViewGrupos.addItemDecoration(itemDecoration);
        adapter = new AdapterGroups(labelEmptyList, getActivity());
        recyclerViewGrupos.setAdapter(adapter);
        FontUtil.setFontBook(getActivity(), labelEmptyList);
        FloatingActionButton fabButton = (FloatingActionButton) getView().findViewById(R.id.btnAddGroup);
        fabButton.setBackgroundTintList(
                getResources().getColorStateList(R.color.rojo));
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupActionDialog dialogGroupAction = GroupActionDialog.newInstance(getActivity());
                dialogGroupAction.setOnGroupActionSuccessListener(new OnGroupActionSuccessListener() {
                    @Override
                    public void onSuccess() {
                        updateGroups();
                    }
                });

                dialogGroupAction.setCancelable(false);
                dialogGroupAction.show(getFragmentManager(), "tagGrupo");
            }
        });
        updateGroups();
    }

    public void updateGroups() {
        ((AdapterGroups) adapter).updateGroups();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateGroups();
    }


}
