package com.alertmass.appalertmass.alertmass.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Channel;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Adapter for channels belonging to a previously selected category.
 */
public class AdapterCategoryChannels extends RecyclerView.Adapter {

    private final List<Integer> subscribedCount;
    private List<ParseObject> categoryChannels;

    /**
     * Listener for the checkbutton of each of the channels belonging to a previously selected category
     */
    private CompoundButton.OnCheckedChangeListener buttonListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(final CompoundButton buttonView, boolean isChecked) {
            final ParseObject idbtn = (ParseObject) buttonView.getTag();
            final View vw = buttonView.getRootView();
            if (isChecked) {
                ParsePush.subscribeInBackground("as" + idbtn.getObjectId(), new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Toast.makeText(vw.getContext(), vw.getContext().getString(R.string.channel_subscribe_fail) + " " + idbtn.getString(Channel.NAME), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(vw.getContext(), vw.getContext().getString(R.string.channel_have_subscribed) + " " + idbtn.getString(Channel.NAME), Toast.LENGTH_SHORT).show();
                            ParseLocalStorage.saveChannelObject(idbtn);
                        }
                    }

                });
            } else {
                //Remover canal
                ParsePush.unsubscribeInBackground("as" + idbtn.getObjectId(), new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Toast.makeText(vw.getContext(), vw.getContext().getString(R.string.channel_unsubscribe_fail) + " " + idbtn.getString(Channel.NAME), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(vw.getContext(), vw.getContext().getString(R.string.channel_have_unsubscribed) + " " + idbtn.getString(Channel.NAME), Toast.LENGTH_SHORT).show();
                            ParseLocalStorage.removeChannel(idbtn.getObjectId());
                        }
                    }

                });
            }
        }
    };

    public AdapterCategoryChannels(List<ParseObject> categoryChannels, List<Integer> subscribedCount) {
        this.categoryChannels = categoryChannels;
        this.subscribedCount = subscribedCount;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_suscanales, parent, false);
        return new CanalesCategoriaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ParseObject canalCategoria = categoryChannels.get(position);
        int numeroSuscritos = subscribedCount.get(position);
        ((CanalesCategoriaViewHolder) holder).bindCanalCategoria(canalCategoria, numeroSuscritos);
    }

    @Override
    public int getItemCount() {
        return categoryChannels.size();
    }

    class CanalesCategoriaViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView subtitle;
        ImageView image;
        TextView count;
        CheckBox btnSubscribe;

        public CanalesCategoriaViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txtTitleChannel);
            subtitle = (TextView) itemView.findViewById(R.id.txtSubtitleChannel);
            btnSubscribe = (CheckBox) itemView.findViewById(R.id.btnSubscribe);
            image = (ImageView) itemView.findViewById(R.id.imgChannel);
            count = (TextView) itemView.findViewById(R.id.txtNumberUsers);
            FontUtil.setFontLight(itemView.getContext(), title);
            FontUtil.setFontBook(itemView.getContext(), subtitle, count);
        }

        public void bindCanalCategoria(final ParseObject canalCategoria, int numeroSuscritos) {
            title.setText(canalCategoria.getString(Channel.NAME));
            subtitle.setText(canalCategoria.getString(Channel.DESCRIPTION));
            btnSubscribe.setTag(canalCategoria);
            btnSubscribe.setOnCheckedChangeListener(buttonListener);
            //String subscribersCount = itemView.getResources().getString(R.string.channel_subscribed_count) + " " + String.valueOf(numeroSuscritos);
            //count.setText(subscribersCount);
            image.setTag(canalCategoria);
            Picasso.with(image.getContext())
                    .load(AppConfig.CHANNEL_IMAGE_SERVER + canalCategoria.getObjectId())
                    .transform(new CircleTransform(CircleTransform.NO_BORDER))
                    .placeholder(R.drawable.placeholder)
                    .into(image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnSubscribe.isChecked()) {
                        btnSubscribe.setChecked(false);
                    } else {
                        btnSubscribe.setChecked(true);
                    }
                }
            });
        }
    }
}
