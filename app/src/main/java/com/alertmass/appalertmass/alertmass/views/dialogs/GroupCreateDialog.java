package com.alertmass.appalertmass.alertmass.views.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.BitmapUtil;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.ExifUtil;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.alertmass.appalertmass.alertmass.views.OnGroupActionSuccessListener;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Dialog for the creation of groups.
 */
public class GroupCreateDialog extends DialogFragment implements View.OnClickListener {
    private static final String TAG = GroupCreateDialog.class.getName();
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1;
    private static final int SELECT_FILE = 2;
    OkHttpClient client = new OkHttpClient();
    private EditText creator;
    private ImageView imgCreator;
    private EditText name;
    private EditText description;
    private EditText email;
    private ImageView img;
    private Context context;
    private OnGroupActionSuccessListener listener;
    private Uri imgUri;

    private TextView emailInfo;

    private ParseObject group;
    private String imgPath;

    public static GroupCreateDialog newInstance(Context context) {
        GroupCreateDialog dialogCreateGroup = new GroupCreateDialog();
        dialogCreateGroup.context = context;
        return dialogCreateGroup;
    }

    public void setOnSuccessListener(OnGroupActionSuccessListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_create_group, null);
        Button btnCreate = (Button) view.findViewById(R.id.btnCreate);
        Button btnCancel = (Button) view.findViewById(R.id.btnClose);
        emailInfo = (TextView) view.findViewById(R.id.txtEmailHelpLink);
        emailInfo.setOnClickListener(this);
        img = (ImageView) view.findViewById(R.id.imgLogo);
        description = (EditText) view.findViewById(R.id.txtDescription);
        name = (EditText) view.findViewById(R.id.txtGroupName);
        email = (EditText) view.findViewById(R.id.txtEmail);
        img = (ImageView) view.findViewById(R.id.imgLogo);

        /**
         * load email preferences
         */
        loadPreferences();

        FontUtil.setFontBook(getActivity() ,name, description, email, btnCreate, btnCancel);

        img.setOnClickListener(this);
        btnCreate.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }
        });
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    /**
     * load email preferences
     */

    public void loadPreferences(){

        SharedPreferences prefsEmail = getActivity().getSharedPreferences("correo", Context.MODE_PRIVATE);
        email.setText(prefsEmail.getString("correo",""));
    }

    public boolean uploadGroupImage(String serverURL, Uri uri, String filename) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            Bitmap bitmap = BitmapUtil.decodeSampledBitmapFromResourceMemOpt(inputStream, 150, 150);
            bitmap = ExifUtil.rotateBitmap(imgPath, bitmap);
            Log.i(TAG, "uploadGroupImage: " + bitmap.getWidth() + " " + bitmap.getHeight());
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
            byte[] bitmapData = bytes.toByteArray();
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file", filename, RequestBody.create(MediaType.parse("image/jpeg"), bitmapData))
                    .build();

            Request request = new Request.Builder()
                    .header("Content-Type", "application/json")
                    .url(serverURL)
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "onFailure: ", e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.i(TAG, "onResponse: " + response.body().string());
                    if (listener != null) {
                        ((Activity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onSuccess();
                            }
                        });

                    }
                }
            });

            return true;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "uploadGroupImage: ", e);
        }
        return false;
    }

    private void createGroup(String groupName, String groupDescription, String groupEmail) throws ParseException {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(getResources().getString(R.string.loading));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        final String id = ParseInstallation.getCurrentInstallation().getObjectId() + timeStamp;
        group = new ParseObject(Group.TABLE_NAME);
        group.put(Group.ID, id);
        group.put(Group.NAME, groupName);
        group.put(Group.IS_ADMIN, true);
        group.put(Group.DESC, groupDescription);
        group.put(Group.EMAIL, groupEmail);
        group.put(Group.STATUS, true);
        group.addUnique(Group.ID_INSTALL, ParseInstallation.getCurrentInstallation().getObjectId());
        ParseLocalStorage.saveGroupObject(group);
        group.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                pDialog.dismiss();
                dismiss();
                if (e == null) {
                    uploadGroupImage(AppConfig.GROUP_IMAGE_POST_SERVER, imgUri, "as" + id);
                    ParsePush.subscribeInBackground("as" + id);
//                    try {
//                        inviteUser(group);
//                    } catch (JSONException e1) {
//                        Log.e(TAG, "done: ", e);
//                    }
                    listener.onSuccess();
                } else {
                    Log.e(TAG, "done: ", e);
                }
            }
        });
    }

    private void chooseFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }

    /**
     * Creates empty image file. Launches camera app to capture image.
     */
    private void captureImage() {
        File image = createImageFile();
        imgUri = Uri.fromFile(image);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Creates empty image file in external storage
     *
     * @return File
     */
    private File createImageFile() {
        // External location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                AppConfig.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + AppConfig.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");


        return mediaFile;
    }

//    private void inviteUser(ParseObject group) throws JSONException {
//        OkHttpClient client = new OkHttpClient();
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("idgrupo", group.getString(Group.ID));
//        jsonObject.put("nomgrupo", group.getString(Group.NAME));
//        jsonObject.put("desgrupo", group.getString(Group.DESC));
//        jsonObject.put("email", group.getString(Group.EMAIL));
//        jsonObject.put("nomper", "Administrador de grupo " + group.getString(Group.NAME));
//        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
//        Request request = new Request.Builder()
//                .addHeader("Content-type", "application/json")
//                .url("http://apiapp-invita.us-east-1.elasticbeanstalk.com")
//                .post(body)
//                .build();
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.e(TAG, "onFailure: ", e);
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                Log.i(TAG, "onResponse: " + response.body().string());
//                response.body().close();
//            }
//        });
//
//    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                imgPath = imgUri.getPath();
                Picasso.with(getActivity())
                        .load(imgUri)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            }
        } else if (requestCode == SELECT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                imgUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(imgUri,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgPath = cursor.getString(columnIndex);
                cursor.close();
                Picasso.with(getActivity())
                        .load(imgUri)
                        .transform(new CircleTransform(CircleTransform.NO_BORDER))
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(img);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgLogo:
                GroupImageDialog groupImageDialog = new GroupImageDialog();
                groupImageDialog.setCameraOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        captureImage();
                    }
                });
                groupImageDialog.setGalleryOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseFromGallery();
                    }
                });
                groupImageDialog.show(getFragmentManager(), "tagGrupo");
                break;
            case R.id.btnCreate:
                if (NetworkUtil.checkEnabledInternet(getActivity())) {
                    String groupName = name.getText().toString();
                    String groupDescription = description.getText().toString();
                    String groupEmail = email.getText().toString();

                    //save email name

                    SharedPreferences prefsEmail = getActivity().getSharedPreferences("correo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorEmail = prefsEmail.edit();
                    String eValor = email.getText().toString();
                    editorEmail.putString("correo",eValor);
                    editorEmail.commit();

                    if (groupName.isEmpty() || groupDescription.isEmpty() || groupEmail.isEmpty()) {
                        Toast.makeText(getActivity(), R.string.fill_fields, Toast.LENGTH_SHORT).show();
                    } else if (imgUri == null) {
                        Toast.makeText(getActivity(), R.string.add_image, Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            createGroup(groupName, groupDescription, groupEmail);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(context, R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnClose:
                dismiss();
                break;
            case R.id.txtEmailHelpLink:

                }

        }
    }




