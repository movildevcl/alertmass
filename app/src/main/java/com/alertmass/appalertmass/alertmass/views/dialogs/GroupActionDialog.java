package com.alertmass.appalertmass.alertmass.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.views.OnGroupActionSuccessListener;

/**
 * Created by benjamin on 1/27/16.
 */
public class GroupActionDialog extends DialogFragment {
    private Context context;
    private OnGroupActionSuccessListener onGroupActionSuccessListener;

    public static GroupActionDialog newInstance(Context context) {
        GroupActionDialog fragment = new GroupActionDialog();
        fragment.setContext(context);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        GroupJoinDialog joinDialog = GroupJoinDialog.newInstance(context);
        joinDialog.setOnSuccessListener(onGroupActionSuccessListener);
//                joinDialog.setCancelable(false);
        joinDialog.show(getFragmentManager(), "tagGrupo");
        dismiss();

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_group_options, null);
        Button btnJoin = (Button) view.findViewById(R.id.btnJoin);
        //Button btnCreate = (Button) view.findViewById(R.id.btnCreate);

        Button btnCancel = (Button) view.findViewById(R.id.btnClose);
        //btnJoin.setOnClickListener(this);
        //btnCreate.setOnClickListener(this);
        //btnCancel.setOnClickListener(this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }
        });
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public void setOnGroupActionSuccessListener(OnGroupActionSuccessListener listener) {
        this.onGroupActionSuccessListener = listener;
    }

    /*@Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnJoin:
                GroupJoinDialog joinDialog = GroupJoinDialog.newInstance(context);
                joinDialog.setOnSuccessListener(onGroupActionSuccessListener);
//                joinDialog.setCancelable(false);
                joinDialog.show(getFragmentManager(), "tagGrupo");
                dismiss();
                break;
        *//*    case R.id.btnCreate:
                GroupCreateDialog createDialog = GroupCreateDialog.newInstance(context);
                createDialog.setOnSuccessListener(onGroupActionSuccessListener);
//                createDialog.setCancelable(false);
                createDialog.show(getFragmentManager(), "tagGrupo");
                dismiss();
                break;*//*
            case R.id.btnClose:
                dismiss();
                break;
        }
    }*/
}
