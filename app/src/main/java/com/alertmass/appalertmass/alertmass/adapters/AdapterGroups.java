package com.alertmass.appalertmass.alertmass.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Adapter for subscribed groups' RecyclerView
 */
public class AdapterGroups extends RecyclerView.Adapter {

    private TextView emptyView;
    private Context context;
    private List<ParseObject> groups;
    private File directory;
    private File myImageFile;
    private SharedPreferences prefs;
    private String groupId;

    public AdapterGroups(TextView emptyView, Context context) {
        this.emptyView = emptyView;
        this.context = context;
        this.groups = ParseLocalStorage.getGroups();
    }

    private Target picassoImageTarget(Context context, final String imageDir, final String imageName) {

        ContextWrapper cw = new ContextWrapper(context);
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName);
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            prefs.edit().putBoolean(groupId, true).apply();
                        } catch (IOException e) {
                            prefs.edit().putBoolean(groupId, false).apply();
                            e.printStackTrace();
                        } finally {
                            try {
                                if (fos != null) {
                                    fos.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                prefs.edit().putBoolean(groupId, false).apply();
                            }
                        }
                        Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());

                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable == null) {
                }
            }
        };
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_grupos, parent, false);
        return new GroupsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        groupId = groups.get(position).getString(Group.ID);
        ((GroupsViewHolder) holder).bindGrupo(groupId);
    }

    public void updateGroups() {
        groups = ParseLocalStorage.getGroups();
        Collections.sort(groups, new Comparator<ParseObject>() {
            @Override
            public int compare(ParseObject lhs, ParseObject rhs) {
                return lhs.getString(Group.NAME).compareTo(rhs.getString(Group.NAME));
            }
        });
        notifyDataSetChanged();
        checkEmptyness();
    }

    private void checkEmptyness() {
        if (getItemCount() == 0 && emptyView.getVisibility() == View.GONE) {
            emptyView.setVisibility(View.VISIBLE);
        } else if (getItemCount() > 0 && emptyView.getVisibility() == View.VISIBLE) {
            emptyView.setVisibility(View.GONE);
        }
    }

    public void unsuscribeGroup(final ParseObject group) {
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.loading));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        ParsePush.unsubscribeInBackground("as" + group.getString(Group.ID), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                try {
                    if (e != null) {
                        Toast.makeText(context, context.getString(R.string.group_subscribe_fail) + " " + group.getString(Group.NAME), Toast.LENGTH_SHORT).show();
                    } else {
                        File fileDelete = new File(directory, group.getString(Group.ID) + "my_image.jpeg");
                        boolean deleted = fileDelete.delete();
                        if (deleted){
                            prefs.edit().putBoolean(group.getString(Group.ID), false).apply();
                        }
                        Toast.makeText(context, context.getString(R.string.group_have_unsubscribed) + " " + group.getString(Group.NAME), Toast.LENGTH_SHORT).show();
                        ParseLocalStorage.removeGroup(group.getString(Group.ID));
                        updateGroups();
                    }
                }catch (Exception ex){
                    Log.e("CapturedError", String.valueOf(ex));
                }
                pDialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

    class GroupsViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView title;
        TextView subtitle;
        ImageButton delete;

        public GroupsViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imgGrupo);
            title = (TextView) itemView.findViewById(R.id.txtGrupo);
            subtitle = (TextView) itemView.findViewById(R.id.txtSubGrupo);
            delete = (ImageButton) itemView.findViewById(R.id.btnEliminarGrupo);
        }

        public void bindGrupo(final String groupId) {
            final ParseObject group = ParseLocalStorage.getGroupById(groupId);
            if (group != null) {

                prefs = PreferenceManager.getDefaultSharedPreferences(context);
                Boolean statusImage = prefs.getBoolean(groupId, false);
                if(statusImage){
                    try {
                        ContextWrapper cw = new ContextWrapper(context);
                        try {
                            directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                            myImageFile = new File(directory, groupId + "my_image.jpeg");
                            Log.d("nombreArchivo", groupId + "my_image.jpeg");
                        }catch (Exception e){
                            Log.e("noencuentraimagen", e.toString());
                        }

                        Picasso.with(context).load(myImageFile)
                                .transform(new CircleTransform(CircleTransform.NO_BORDER))
                                .fit()
                                .centerCrop()
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .into(img);
                    }catch (Exception e){
                        Picasso.with(context).load(AppConfig.GROUP_IMAGE_GET_SERVER + "as" + groupId)
                                .into(picassoImageTarget(context, "imageDir", groupId + "my_image.jpeg"));

                        Picasso.with(context)
                                .load(AppConfig.GROUP_IMAGE_GET_SERVER + "as" + groupId)
                                .transform(new CircleTransform(CircleTransform.NO_BORDER))
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .fit()
                                .centerCrop()
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .into(img);
                        prefs.edit().putBoolean(groupId, false).apply();
                    }
                }else {

                    Picasso.with(context).load(AppConfig.GROUP_IMAGE_GET_SERVER + "as" + groupId)
                            .into(picassoImageTarget(context, "imageDir", groupId + "my_image.jpeg"));

                    Picasso.with(context)
                            .load(AppConfig.GROUP_IMAGE_GET_SERVER + "as" + groupId)
                            .transform(new CircleTransform(CircleTransform.NO_BORDER))
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .fit()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            .into(img);
                }
                title.setText(group.getString(Group.NAME));
                subtitle.setText(group.getString(Group.DESC));
                delete.setTag(group);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {

                        new AlertDialog.Builder(context)
                                .setTitle(group.getString(Group.NAME))
                                .setMessage(context.getString(R.string.delete_group_dialog))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (NetworkUtil.checkEnabledInternet(v.getContext())) {
                                            final ParseObject idbtn = (ParseObject) v.getTag();
                                            unsuscribeGroup(idbtn);
                                        } else {
                                            Toast.makeText(v.getContext(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
            }
    }
}}
