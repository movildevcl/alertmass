package com.alertmass.appalertmass.alertmass.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.adapters.AdapterCategoryChannels;
import com.alertmass.appalertmass.alertmass.adapters.DividerItemDecoration;
import com.alertmass.appalertmass.alertmass.data.Category;
import com.alertmass.appalertmass.alertmass.data.Channel;
import com.alertmass.appalertmass.alertmass.database.ParseLocalStorage;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity that manages the view for the channels belonging to a previously selected category.
 */
public class ChannelsCategoryActivity extends Activity {

    private static final String TAG = ChannelsCategoryActivity.class.getSimpleName();
    private RecyclerView recyclerViewCanalesCategoria;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView categoriesError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe_channel);
        overridePendingTransition(R.anim.left_in, R.anim.left_out);

        ImageButton back = (ImageButton) findViewById(R.id.imgBtnBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        recyclerViewCanalesCategoria = (RecyclerView) findViewById(R.id.recViewCategoryChannels);
        layoutManager = new LinearLayoutManager(this);
        recyclerViewCanalesCategoria.setLayoutManager(layoutManager);
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        recyclerViewCanalesCategoria.addItemDecoration(itemDecoration);
        Bundle bundle = getIntent().getExtras();
        String categoryName = bundle.getString(Category.NAME);
        String categoryId = bundle.getString(Channel.CATEGORY_ID);
        TextView categoryTitle = (TextView) findViewById(R.id.txtCategoryTitle);
        categoriesError = (TextView) findViewById(R.id.txtCategoryChannelsError);
        categoryTitle.setText(categoryName);
        FontUtil.setFontBook(this, categoryTitle, categoriesError);
        if (NetworkUtil.checkEnabledInternet(this)) {
            new LoadChannelsTask(this).execute(categoryId);
        } else {
            categoriesError.setVisibility(View.VISIBLE);
        }
    }

    private List<ParseObject> filterSubscribedChannels(List<ParseObject> channels) {
        List<ParseObject> subscribedChannels = ParseLocalStorage.getChannels();
        List<ParseObject> filteredChannels = new ArrayList<>(channels);
        for (ParseObject channel : channels) {
            for (ParseObject subscribedChannel : subscribedChannels) {
                if (channel.getObjectId().equals(subscribedChannel.getObjectId())) {
                    filteredChannels.remove(channel);
                }
            }
        }
        return filteredChannels;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Loads channels corresponding to a selected category. It only shows channels the user has not subscribed yet.
     */
    private class LoadChannelsTask extends AsyncTask<String, Void, Boolean> {

        private List<Integer> subscribedCount = new ArrayList<>();
        private List<ParseObject> notSubscribedChannels;
        private ProgressDialog pDialog;

        public LoadChannelsTask(Context context) {
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage(getString(R.string.loading));
            pDialog.setIndeterminate(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... categoryId) {

            try {
                ParseQuery<ParseObject> queryCanales = ParseQuery.getQuery(Channel.TABLE_NAME);
                queryCanales.whereEqualTo(Channel.CATEGORY_ID, categoryId[0]);
                queryCanales.whereEqualTo(Channel.STATUS, true);
                List<ParseObject> list = queryCanales.find();
                notSubscribedChannels = filterSubscribedChannels(list);
                int count;
                for (ParseObject channel : notSubscribedChannels) {
                    count = Channel.getSubscribersCount(channel.getObjectId());
                    subscribedCount.add(count);
                }

            } catch (ParseException e) {
                Log.e(TAG, "doInBackground: ", e);
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                pDialog.dismiss();
                adapter = new AdapterCategoryChannels(notSubscribedChannels, subscribedCount);
                recyclerViewCanalesCategoria.setAdapter(adapter);
                categoriesError.setVisibility(View.GONE);
            } else {
                categoriesError.setVisibility(View.VISIBLE);
            }
        }
    }
}
