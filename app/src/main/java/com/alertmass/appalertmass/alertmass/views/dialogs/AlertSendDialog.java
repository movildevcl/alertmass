package com.alertmass.appalertmass.alertmass.views.dialogs;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Alert;
import com.alertmass.appalertmass.alertmass.data.Group;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.parse.ParseException;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by benjamin on 2/12/16.
 */
public class AlertSendDialog extends DialogFragment implements View.OnClickListener {
    private static final String TAG = AlertSendDialog.class.getSimpleName();
    private Button btnCancel;
    private Button btnSend;
    private EditText alert;
    private ParseObject group;


    public static AlertSendDialog newInstance(ParseObject group) {
        AlertSendDialog alertSendDialog = new AlertSendDialog();
        alertSendDialog.group = group;
        return alertSendDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_send_alert, null);
        alert = (EditText) view.findViewById(R.id.txtAlert);
        btnSend = (Button) view.findViewById(R.id.btnSend);
        btnCancel = (Button) view.findViewById(R.id.btnClose);
        btnSend.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }
        });
        FontUtil.setFontBook(getActivity(), alert, btnSend, btnCancel);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    private String sendAlert(String groupId, String data) throws ParseException, JSONException, IOException {
//        HashMap<String, String> params = new HashMap<>();
//        params.put("groupId", groupId);
//        params.put("installationId", ParseInstallation.getCurrentInstallation().getObjectId());
//        params.put("message", json);
//        return ParseCloud.callFunction("sendPushToGroup", params);
        OkHttpClient client = new OkHttpClient();
        JSONObject alertJson = new JSONObject();
        JSONObject channels = new JSONObject();
        JSONArray in = new JSONArray();
        in.put("as"+groupId);
        channels.put("$in", in);
        JSONObject where = new JSONObject();
        where.put("channels", channels);
        alertJson.put("where", where);
        JSONObject dataJson = new JSONObject(data);
        alertJson.put("data", dataJson);
        Log.i(TAG, "sendAlert: " + alertJson.toString());
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), alertJson.toString());
        Request request = new Request.Builder()
                .addHeader("X-Parse-Application-Id", AppConfig.PARSE_APPLICATION_ID)
                .addHeader("X-Parse-Master-Key", "bncJq5uDOuZYHaySpGntIRTzqFfgAb8XQ6qeYlaL")
                .addHeader("Content-Type", "application/json")
                .post(body)
                .url(AppConfig.SERVER + "push")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    //TODO: Asegurarse que el formato de fecha y hora se mantiene por siempre
    private String makeAlert(String msg) {
        JSONObject alert = new JSONObject();
        Date date = new Date();
        SimpleDateFormat dateFormat = Alert.DATE_FORMAT_INPUT;
        SimpleDateFormat timeFormat = Alert.TIME_FORMAT_INPUT;
        try {
            alert.put(Alert.DATE, dateFormat.format(date));
            alert.put(Alert.HOUR, timeFormat.format(date));
            alert.put(Alert.CHANNEL_NAME, group.get(Group.NAME));
            alert.put(Alert.CHANNEL_ID, group.get(Group.ID));
            alert.put(Alert.MSG, msg);
        } catch (JSONException e) {
            Log.e(TAG, "makeAlert: ", e);
            return "";
        }
        return alert.toString();
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                if (NetworkUtil.checkEnabledInternet(view.getContext())) {
                    new AsyncTask<String, Void, Boolean>() {
                        ProgressDialog pDialog;

                        @Override
                        protected void onPreExecute() {
                            pDialog = new ProgressDialog(view.getContext());
                            pDialog.setMessage(view.getContext().getString(R.string.sending));
                            pDialog.setIndeterminate(false);
                            pDialog.setCancelable(false);
                            pDialog.show();
                        }

                        @Override
                        protected Boolean doInBackground(String... alertMsg) {
                            try {
                                String response = sendAlert(group.getString(Group.ID), alertMsg[0]);
                                Log.i(TAG, "doInBackground: "+response);
                                return true;
                            } catch (ParseException | JSONException | IOException e) {
                                Log.e(TAG, "onClick: ", e);
                                return false;
                            }
                        }

                        @Override
                        protected void onPostExecute(Boolean success) {
                            pDialog.dismiss();
                            dismiss();
                            if (success) {
                                Toast.makeText(view.getContext(), R.string.alert_send_success, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(view.getContext(), R.string.alert_send_error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.execute(makeAlert(alert.getText().toString()));
                } else {
                    Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnClose:
                dismiss();
                break;
        }
    }

}
