package com.alertmass.appalertmass.alertmass.data;

/**
 * DataHolder class that enables transfering objects between activities.
 */
public class DataHolder {
    private static final DataHolder holder = new DataHolder();
    private Object data;

    public static DataHolder getInstance() {
        return holder;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
