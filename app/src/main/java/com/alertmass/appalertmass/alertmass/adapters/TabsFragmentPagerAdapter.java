package com.alertmass.appalertmass.alertmass.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.views.tabs.AlertsTabFragment;
import com.alertmass.appalertmass.alertmass.views.tabs.ChannelTabFragment;
import com.alertmass.appalertmass.alertmass.views.tabs.GroupTabFragment;

/**
 * Created by Pablo on 11-09-15.
 */
public class TabsFragmentPagerAdapter extends FragmentPagerAdapter {

    public AlertsTabFragment alertsFragment = new AlertsTabFragment();
    public ChannelTabFragment channelFragment = new ChannelTabFragment();
    public GroupTabFragment groupFragment = new GroupTabFragment();
    public Fragment[] fragments = new Fragment[]{alertsFragment, channelFragment, groupFragment};
    Context context;
    private String tabTitles[];

    public TabsFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        String alerts = context.getResources().getString(R.string.alerts);
        String channels = context.getResources().getString(R.string.channels);
        String groups = context.getResources().getString(R.string.groups);
        tabTitles = new String[]{alerts, channels, groups};
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        // Generate title based on item position
        return tabTitles[position];

      /*  Drawable image = ContextCompat.getDrawable(context,imageResId[position]);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/
/*
        return sb;
*/
    }
}
