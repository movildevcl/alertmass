package com.alertmass.appalertmass.alertmass;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alertmass.appalertmass.alertmass.adapters.TabsFragmentPagerAdapter;
import com.alertmass.appalertmass.alertmass.data.Country;
import com.alertmass.appalertmass.alertmass.services.AlertMassService;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.util.NetworkUtil;
import com.alertmass.appalertmass.alertmass.views.dialogs.CountryDialog;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity implements CountryDialog.OnCountrySelectListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private CountryDialog countryDialog;
    private IntentFilter intentFilter;
    private ViewPager viewPager;
    private DrawerLayout drawerLayout;
    private Boolean yourLocked;
    private OkHttpClient client;
    private String currentInstallationiD;
    private SharedPreferences prefs;
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("Receiver", "Update");
            ((((TabsFragmentPagerAdapter) viewPager.getAdapter()).alertsFragment)).notifyAdapter();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        yourLocked = prefs.getBoolean("primerInicio", false);
        client = new OkHttpClient();
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new TabsFragmentPagerAdapter(getSupportFragmentManager(), this));
        TabLayout tabLayout = (TabLayout) findViewById(R.id.appbarTabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
        //Start service
        Intent intent = new Intent(this, AlertMassService.class);
        startService(intent);

        intentFilter = new IntentFilter();
        intentFilter.addAction("CL.ALERTMASS.PUSH");
        ImageButton map = (ImageButton) findViewById(R.id.imgBtnMap);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.checkEnabledInternet(v.getContext())) {
                    getCountries();
                }
                else {
                    Toast.makeText(v.getContext(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //Set fonts
        TextView subtitle = (TextView) findViewById(R.id.txtSubtitle);
        TextView website = (TextView) findViewById(R.id.txtWebsite);
        TextView mail = (TextView) findViewById(R.id.txtEmail);
        TextView copyright = (TextView) findViewById(R.id.txtCopyright);
        TextView title = (TextView) findViewById(R.id.txtAppTitle);
        TextView createdBy = (TextView) findViewById(R.id.txtCreatedBy);
        FontUtil.setFontHeavy(this, createdBy);
        FontUtil.setFontMedium(this, subtitle, website, mail);
        FontUtil.setFontBook(this, copyright, title);
        AppConfig.country = Country.getCurrentCountry(this);
        AppConfig.countryId = Country.getCurrentCountryId(this);
        isRegistered();
    }


    private void isRegistered(){

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                    if (!yourLocked) {

                        final String idAndroid = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                Settings.Secure.ANDROID_ID);

                        currentInstallationiD = String.valueOf(ParseInstallation.getCurrentInstallation().get("installationId"));
                        Log.d("InstallationId", currentInstallationiD);
                        ParseInstallation.getCurrentInstallation().put("idAndroid", idAndroid);
                        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Log.i(TAG, "Androidid agregado: " + idAndroid);
                                    prefs.edit().putBoolean("primerInicio", true).apply();

                                    JSONObject json = new JSONObject();
                                    JSONObject objectId = new JSONObject();
                                    try {
                                        json.put("objectId", objectId);
                                        objectId.put("$ne", currentInstallationiD);
                                        json.put("idAndroid", idAndroid);
                                        Log.d("jsonfinal", json.toString());
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                        prefs.edit().putBoolean("primerInicio", false).apply();
                                    }
                                    try {
                                        postJSON(AppConfig.INSTALLATIONS_GET, json);
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                        prefs.edit().putBoolean("primerInicio", false).apply();
                                    }
                                } else {
                                    Log.e(TAG, "Error guardando androidId!", e);
                                    prefs.edit().putBoolean("primerInicio", false).apply();
                                }
                            }
                        });
                    } else {
                        Log.d("Existe id", "existe sesion iniciada");
                    }
                    }catch (Exception ignored){
                    }
                }
            });
            thread.start();

    }

    private void postJSON(String url, JSONObject json) throws IOException {

        Log.d("URLFINAL", url+json);

        Request request = new Request.Builder()
                .addHeader("content-type", "application/json")
                .addHeader("x-parse-application-id", "1FPKiWd4niGvmuFEistvBzy8QdEKRkAapW9E1ZNy")
                .addHeader("x-parse-master-key", "bncJq5uDOuZYHaySpGntIRTzqFfgAb8XQ6qeYlaL")
                .url(url + json)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: ", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, "onResponse post json: " + response.toString());
                final String responseData = response.body().string();
                getJson(responseData);
            }
        });
    }

    private void postJSONChannell(String url){

        Log.d("URLFINAL", url);

        //MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        //RequestBody body = RequestBody.create(JSON, json.toString());

        Request request = new Request.Builder()
                .addHeader("content-type", "application/json")
                .addHeader("x-parse-application-id", "1FPKiWd4niGvmuFEistvBzy8QdEKRkAapW9E1ZNy")
                .addHeader("x-parse-master-key", "bncJq5uDOuZYHaySpGntIRTzqFfgAb8XQ6qeYlaL")
                .delete()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: ", e);
                prefs.edit().putBoolean("primerInicio", false).apply();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, "onResponse put idinstallation: " + response.toString());
                final String responseData = response.body().string();
                getJson(responseData);
                prefs.edit().putBoolean("primerInicio", true).apply();
            }
        });
    }

    private void getJson(String respuesta){
        try {
            JSONObject jsonObjMain = new JSONObject(respuesta);
            JSONArray jsonArray = jsonObjMain.getJSONArray("results");
            Log.d("array",jsonArray.toString());
            for (int i = 0; i <=jsonArray.length(); i++) {
                JSONObject object = (JSONObject) jsonArray.get(i);
                String installationId = object.getString("installationId");
                String objectId;
                if (!installationId.equals(currentInstallationiD)) {
                    objectId = object.getString("objectId");
                    try {
                        postJSONChannell(AppConfig.CLEAN_CHANNELS + objectId);
                    } catch (Exception e) {
                        Log.e("error", e.toString());
                        prefs.edit().putBoolean("primerInicio", false).apply();
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            prefs.edit().putBoolean("primerInicio", false).apply();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(myReceiver, intentFilter);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent main = new Intent(Intent.ACTION_MAIN);
            main.addCategory(Intent.CATEGORY_HOME);
            main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(main);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_settings:
                changeCountry();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void changeCountry() {
        //Cargar paises
        Map<String, String> country = Country.getCountries();
        String[] spinnerArray = new String[country.size()];
        int index = 0;
        for (Map.Entry<String, String> mapEntry : country.entrySet()) {
            spinnerArray[index] = mapEntry.getValue();
            index++;
        }
        Arrays.sort(spinnerArray);

        FragmentManager fragmentManager = MainActivity.this
                .getSupportFragmentManager();
        countryDialog = CountryDialog.newInstance(spinnerArray);
        countryDialog.setCancelable(false);
        countryDialog.show(fragmentManager, "countryDialog");
    }

    @Override
    public void onCountrySelect(final String countryName) {
        countryDialog.dismiss();

        new AsyncTask<Void, Void, Boolean>() {
            ProgressDialog pDialog = new ProgressDialog(MainActivity.this);

            @Override
            protected Boolean doInBackground(Void... params) {
                ParseManager.setCurrentCountry(pDialog.getContext(), countryName);
                return true;
            }

            @Override
            protected void onPreExecute() {
                pDialog.setMessage(getString(R.string.loading));
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                pDialog.dismiss();
            }
        }.execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, intentFilter);
    }

    public void getCountries() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        final Map<String, String> countries = new HashMap<>();
        final ParseQuery<ParseObject> queryPaises = ParseQuery.getQuery(Country.TABLE_NAME);

        queryPaises.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> results, ParseException e) {
                if (e != null) {
                    pDialog.dismiss();
                    Log.e(TAG, "done: ", e);
                    return;
                }
                for (ParseObject country : results) {
                    countries.put(country.getObjectId(), (String) country.get(Country.NAME));
                }

                String[] spinnerArray = new String[countries.size()];
                int index = 0;
                for (Map.Entry<String, String> mapEntry : countries.entrySet()) {
                    spinnerArray[index] = mapEntry.getValue();
                    index++;
                }
                Arrays.sort(spinnerArray);

                FragmentManager fragmentManager = MainActivity.this
                        .getSupportFragmentManager();
                countryDialog = CountryDialog.newInstance(spinnerArray);
                countryDialog.setCancelable(false);

                pDialog.dismiss();

                countryDialog.show(fragmentManager, "tagAlerta");
            }
        });

    }
}
