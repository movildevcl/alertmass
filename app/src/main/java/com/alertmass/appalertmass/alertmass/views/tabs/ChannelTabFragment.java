package com.alertmass.appalertmass.alertmass.views.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.adapters.AdapterChannels;
import com.alertmass.appalertmass.alertmass.adapters.DividerItemDecoration;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.alertmass.appalertmass.alertmass.views.CategoriesActivity;

/**
 * Channels tab
 */
public class ChannelTabFragment extends Fragment implements View.OnClickListener {
    private RecyclerView.Adapter adapter;
    private boolean isChannelAdded = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_channels, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView recyclerViewCanales = (RecyclerView) getView().findViewById(R.id.recViewChannels);
        TextView labelEmptyList = (TextView) getView().findViewById(R.id.txtChannelEmpty);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewCanales.setLayoutManager(layoutManager);
        recyclerViewCanales.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        recyclerViewCanales.addItemDecoration(itemDecoration);
        adapter = new AdapterChannels(labelEmptyList, getActivity());
        recyclerViewCanales.setAdapter(adapter);
        FontUtil.setFontBook(getActivity(), labelEmptyList);
        FloatingActionButton fabButton = (FloatingActionButton) getView().findViewById(R.id.btnAddChannel);
        fabButton.setBackgroundTintList(
                getResources().getColorStateList(R.color.rojo));
        fabButton.setOnClickListener(this);
        ((AdapterChannels) adapter).updateChannels();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isChannelAdded) {
            ((AdapterChannels) adapter).updateChannels();
            isChannelAdded = false;
        }
    }


    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.btnAddChannel:
                isChannelAdded = true;
                Intent intent = new Intent(view.getContext(), CategoriesActivity.class);
                view.getContext().startActivity(intent);
//                    final ProgressDialog pDialog = new ProgressDialog(view.getContext());
//                    pDialog.setMessage(view.getResources().getString(R.string.loading));
//                    pDialog.setIndeterminate(false);
//                    pDialog.setCancelable(false);
//                    pDialog.show();
//                    ParseQuery<ParseObject> queryCategorias = ParseQuery.getQuery("categorias");
//                    queryCategorias.whereEqualTo("idpais", Country.getCurrentCountryId());
//                    queryCategorias.findInBackground(new FindCallback<ParseObject>() {
//                        @Override
//                        public void done(List<ParseObject> list, ParseException e) {
//                            if (e != null) {
//                                e.printStackTrace();
//                            }
//                            pDialog.dismiss();
//                            Intent intent = new Intent(view.getContext(), CategoriesActivity.class);
//                            DataHolder.getInstance().setData(list);
//                            view.getContext().startActivity(intent);
//                        }
//                    });
                break;
        }
    }
}
