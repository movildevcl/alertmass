package com.alertmass.appalertmass.alertmass.views;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.alertmass.appalertmass.alertmass.AppConfig;
import com.alertmass.appalertmass.alertmass.R;
import com.alertmass.appalertmass.alertmass.data.Alert;
import com.alertmass.appalertmass.alertmass.data.DataHolder;
import com.alertmass.appalertmass.alertmass.util.CircleTransform;
import com.alertmass.appalertmass.alertmass.util.FontUtil;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Shows alert content. Plays media, if present. Shows and manages survey, if present.
 */
public class AlertMessageActivity extends Activity {

    private final String TAG = "AlertMessageActivity";

    private boolean isSurvey = false;
    private String optionSelected;
    private Button playButton;
    private SeekBar seekBar;
    private RadioGroup optionsRadioGroup;
    private ProgressBar progressBar;
    private String surveyAnswer;
    private ParseObject alert;
    private MediaPlayer mediaPlayer;
    private ProgressBar progressBarAudio;
    private OkHttpClient client;
    private String substr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_detail);
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        client = new OkHttpClient();
        alert = (ParseObject) DataHolder.getInstance().getData();
        ImageView imgAlert = (ImageView) findViewById(R.id.imgAlert);
        TextView channelName = (TextView) findViewById(R.id.txtChannelName);
        TextView body = (TextView) findViewById(R.id.txtBodyAlert);
        TextView date = (TextView) findViewById(R.id.txtDatetime);
        Button msgBtn = (Button) findViewById(R.id.btnAnswerSurvey);

        if (alert.get(Alert.OPTIONS).toString().isEmpty()){
            msgBtn.setVisibility(View.GONE);
        }

        ImageView channelImg = (ImageView) findViewById(R.id.imgChannelAlert);
        playButton = (Button) findViewById(R.id.btnAudioPlay);
        seekBar = (SeekBar) findViewById(R.id.seekBarAudio);
        optionsRadioGroup = (RadioGroup) findViewById(R.id.radioGrpSurvey);
        ImageButton backBtn = (ImageButton) findViewById(R.id.imgBtnBack);
        //Flag alert as read
        alert.put(Alert.IS_READ, true);
        try {
            alert.pin();
        } catch (ParseException e) {
            Log.e(TAG, "Error saving alert to Parse local datastore:", e);
        }
        //Set image
        String foo = alert.get("idcanal").toString();


        substr=foo.substring(2,foo.length());

        if (substr.length()>=9) {
            Picasso.with(getApplicationContext()).load(AppConfig.GROUP_IMAGE_GET_SERVER + substr)
                    .into(picassoImageTarget(getApplicationContext(), "imageDir", substr + "my_image.jpeg"));
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File myImageFile = new File(directory,substr+"my_image.jpeg");

            Picasso.with(getApplicationContext()).load(myImageFile)
                    .transform(new CircleTransform(CircleTransform.NO_BORDER))
                    .fit()
                    .centerCrop()
                    .into(channelImg);
        } else {
            Picasso.with(getApplicationContext())
                    .load(AppConfig.CHANNEL_IMAGE_SERVER + alert.get(Alert.CHANNEL_ID))
                    .transform(new CircleTransform(this.getResources().getColor(R.color.lightgrey)))
                    .fit()
                    .centerCrop()
                    .into(channelImg);
        }
        //Set fonts
        FontUtil.setFontBook(this, channelName, date, body);
        //Set content
        channelName.setText(alert.getString(Alert.CHANNEL_NAME));
        body.setText(alert.getString(Alert.MSG));
        body.setTextColor(Color.BLACK);
        String datetime = alert.getString(Alert.DATE) + " " + alert.getString(Alert.HOUR);
        try {
            date.setText(datetime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        imgAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AlertMessageActivity.this,FullScreenImage.class);
                intent.putExtra("urlImage", alert.getString(Alert.IMG_URL));
                startActivity(intent);
            }
        });
        // If contains image
        if (alert.containsKey(Alert.IMG_URL) && alert.get(Alert.IMG_URL) != null) {
            imgAlert.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(alert.getString(Alert.IMG_URL))
                    .error(R.drawable.placeholder)
                    .into(imgAlert);
        }
        // If contains audio
        if (alert.containsKey(Alert.AUDIO_URL) && alert.get(Alert.AUDIO_URL) != null) {
            playAudio(alert);
        }
        // If contains video
        if (alert.containsKey(Alert.VIDEO_URL) && alert.get(Alert.VIDEO_URL) != null) {
            playVideo(alert);
        }
        // If is survey
        if (alert.containsKey(Alert.OPTIONS) && alert.get(Alert.OPTIONS) != null) {
            showSurvey(alert);
        }

        View.OnClickListener closeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        };
        View.OnClickListener sendListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!optionSelected.isEmpty()) {
                    sendSurveyAnswer(optionSelected);
                    Toast.makeText(getApplicationContext(), R.string.sendresponse,Toast.LENGTH_SHORT).show();
                    finish();
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
                } else {
                    Toast.makeText(view.getContext(), R.string.seleccionealternativa, Toast.LENGTH_SHORT).show();
                }
            }
        };
        if (isSurvey) {
            msgBtn.setText(R.string.send);
            msgBtn.setOnClickListener(sendListener);
        } else {
            msgBtn.setVisibility(View.GONE);
        }
        backBtn.setOnClickListener(closeListener);

    }

    /**
     * Plays audio from url in alert.
     * @param alert Alert
     */
    private void playAudio(ParseObject alert) {
        try {
            progressBarAudio = (ProgressBar) findViewById(R.id.progressBarAudio);
            progressBarAudio.setVisibility(View.VISIBLE);
            mediaPlayer = new MediaPlayer();
            final int playing = 0;
            final int pause = 1;
            playButton.setTag(pause);
            playButton.setText(R.string.play);
            Button.OnClickListener buttonListener = new Button.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if ((int) view.getTag() == playing) {
                        mediaPlayer.pause();
                        view.setTag(pause);
                        ((Button) view).setText(R.string.play);
                    } else {
                        mediaPlayer.start();
                        view.setTag(playing);
                        ((Button) view).setText(R.string.pause);
                    }
                }
            };
            playButton.setOnClickListener(buttonListener);
            SeekBar.OnSeekBarChangeListener seekBarOnSeekChangeListener = new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    if (fromUser) {
                        mediaPlayer.seekTo(progress * 1000);
                        seekBar.setProgress(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            };
            seekBar.setOnSeekBarChangeListener(seekBarOnSeekChangeListener);
            final Handler handler = new Handler();
            AlertMessageActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int mCurrentPosition = mediaPlayer.getCurrentPosition() / 1000;
                    seekBar.setProgress(mCurrentPosition);
                    handler.postDelayed(this, 1000);
                }
            });
            String url = alert.getString(Alert.AUDIO_URL);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    seekBar.setMax(mediaPlayer.getDuration() / 1000);
                    progressBarAudio.setVisibility(View.GONE);
                    LinearLayout audioLayout = (LinearLayout) findViewById(R.id.audioLayout);
                    audioLayout.setVisibility(View.VISIBLE);
                }
            });
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            Log.e(TAG, "onCreate: Error playing audio!", e);
        }
    }

    /**
     * Plays video from URL in alert.
     *
     * @param alert Alert
     */
    private void playVideo(ParseObject alert) {
        Log.i(TAG, "playVideo: ");
        VideoView video = (VideoView) findViewById(R.id.videoAlert);
        progressBar = (ProgressBar) findViewById(R.id.progressBarVideo);
        Uri uri = Uri.parse(alert.getString(Alert.VIDEO_URL));
        MediaController mediaController = new MediaController(this);
        mediaController.setMediaPlayer(video);
        mediaController.setAnchorView(video);
        video.setMediaController(mediaController);
        video.setVideoURI(uri);
        video.requestFocus();
        video.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                progressBar.setVisibility(View.GONE);
            }
        });
        video.start();
    }

    /**
     * Shows survey. Gets options from alert.
     *
     * @param alert Alert
     */
    private void showSurvey(ParseObject alert) {
        // If it has not b
        Log.e("answer to string", String.valueOf(alert.containsKey(Alert.ANSWER)));

        if (!alert.containsKey(Alert.ANSWER)) {
           isSurvey = true;
            RadioButton.OnClickListener radioButtonListener = new RadioButton.OnClickListener() {
                @Override
                public void onClick(View view) {
                    optionSelected = ((RadioButton) view).getText().toString();
                    surveyAnswer = ((RadioButton) view).getText().toString();
                }
            };

            try {
                JSONArray optionsJson = new JSONArray(alert.getString(Alert.OPTIONS));
                int jsonSize = optionsJson.length();
                for (int i = 0; i < jsonSize; i++) {
                    RadioButton radioButton = new RadioButton(this);
                    radioButton.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
                    FontUtil.setFontBook(this, radioButton);
                    optionsRadioGroup.addView(radioButton);
                    radioButton.setText(optionsJson.getString(i));
                    radioButton.setTextColor(Color.BLACK);
                    radioButton.setOnClickListener(radioButtonListener);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // Already answered
        else {
            try {
                JSONArray optionsJson = new JSONArray(alert.getString(Alert.OPTIONS));
                int jsonSize = optionsJson.length();
                for (int i = 0; i < jsonSize; i++) {
                    RadioButton radioButton = new RadioButton(this);
                    radioButton.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
                    FontUtil.setFontBook(this, radioButton);
                    optionsRadioGroup.addView(radioButton);
                    radioButton.setText(optionsJson.getString(i));
                    radioButton.setEnabled(false);
                    if (optionsJson.getString(i).equals(alert.getString(Alert.ANSWER))) {
                        radioButton.setChecked(true);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendSurveyAnswer(String optionSelected) {

        SharedPreferences preferencesMail = PreferenceManager.getDefaultSharedPreferences(this);
        String mail = preferencesMail.getString("Mail","");
        TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        mngr.getDeviceId();
        String idResp = alert.getString(Alert.ID_RESP);
        String cuatro = String.valueOf(4);
        String timeStamp = System.currentTimeMillis() + "";
        String respi;

        if (substr.length()>=9){
            respi = mail;
        }else{
            respi = ParseInstallation.getCurrentInstallation().getObjectId();
        }
        try {
            JSONObject json = new JSONObject();
            json.put("idresp",idResp );
            json.put("respuesta", optionSelected);
            json.put("canal",cuatro);
            json.put("email",respi);
            json.put("datetime",timeStamp);
            alert.put(Alert.ANSWER, surveyAnswer);
            alert.pin();
            Log.i(TAG, "sendSurveyAnswer: " + optionSelected);
            postJSON(AppConfig.SURVEY_RESPONSE_API, json.toString());
            Log.d("jsonresp",json.toString());
        } catch (JSONException | ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    private void postJSON(String url, String json) throws IOException {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .addHeader("Content-Type","application/json")
                .addHeader("x-api-key","HXjrf9QjZq2eXu3FoMq5M1ANyLN00Z2J4vkLVlTB")
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: ", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, "onResponse post json: " + response.toString());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }
    public static Target picassoImageTarget(Context context, final String imageDir, final String imageName) {
        Log.d("picassoImageTarget", " picassoImageTarget");
        ContextWrapper cw = new ContextWrapper(context);
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE); // path to /data/data/yourapp/app_imageDir
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName); // Create image file
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (fos != null) {
                                    fos.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());

                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        };
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
        return super.onKeyDown(keyCode, event);
    }
}
